#ifndef ENEMIESMANAGER_H
#define ENEMIESMANAGER_H

#include "AppIncludes.h"
#include "SnowMan.h"
#include "Game.h"
class SnowMan;

class EnemiesManager
{
public:
	// Constructor
	EnemiesManager(Game* game);

	// Destructor
	~EnemiesManager();

	//Variables
	std::vector<SnowMan*> enemies;

	// Methods
	void generateRegularEnemy();
	void generateStartingEnemies(int nb);
	void moveEnemies();
	void renderEnemies();
	void reset();

private:
	// Constructor
	EnemiesManager();

	// Variables
	long int snowManGenerationTick;

	// Parameters
	Game* game;

	// Methods
	void generateEnemy();
};

#endif // !GAME_H
