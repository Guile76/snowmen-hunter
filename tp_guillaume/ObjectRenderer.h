#ifndef OBJECTRENDERER_H
#define OBJECTRENDERER_H

#include "AppDefines.h"
#include "AppIncludes.h"
#include "Game.h"
#include "sdlglutils.h"

class Game;

class ObjectRenderer
{
public:
	// Constructor
	ObjectRenderer(Game* game);

	// Destructor
	~ObjectRenderer();

	// Methods
	void initialize();
	GLuint getSkybox();
	GLuint getSnowBall();
	GLuint getCylinder();
	GLuint getCone();
	GLuint getDisk();
	GLuint getCarott();
	GLuint getBall();
	GLuint getSnowMan();
	GLuint getCanonRight();
	GLuint getCanonLeft();
	GLuint getTank();
	GLuint getSnowRefill();
	GLuint getPineTree();

private:
	// Consctructor
	ObjectRenderer();

	// Variables
	std::string fichierFloor, fichierBack, fichierForward, fichierLeft, fichierRight, fichierUp;
	GLuint idFloor, idUp, idLeft, idRight, idForward, idBack;
	GLuint idSkybox;
	GLuint idSnowBall, idCylinder, idCone, idDisk, idCarott, idBall, idSnowMan, idCanonRight, idCanonLeft, idTank, idSnowRefill, idPineTree;

	// Parameters
	GLUquadric* q_params;
	Game* game = NULL;

	// Methods
	void initializeSkybox();
	void initializeSnowBall();
	void initializeCylinder();
	void initializeCone();
	void initializeDisk();
	void initializeCarott();
	void initializeBall();
	void initializeSnowMan();
	void initializeCanonRight();
	void initializeCanonLeft();
	void initializeTank();
	void initializeSnowRefill();
	void initializePineTree();
};

#endif // !GAME_H