#ifndef PINETREE_H
#define PINETREE_H

#include "GameObject.h"
#include "Game.h"

class Game;

class PineTree :
	public GameObject
{
public:
	// Consctructors
	PineTree();
	PineTree(Game* game, double radius, double posX, double posY);

	// Destructor
	~PineTree();

	// Method
	void render();

private:
	// Parameter
	Game* game;
};

#endif // !PINETREE_H
