#include "Projectile.h"

Projectile::Projectile()
{
}

Projectile::Projectile(double x, double y, double z, double angle, Game * game, double radius) : GameObject(radius)
{
	this->game = game;
	this->posX = x;
	this->posY = y;
	this->posZ = z;
	setSize();
	this->angle = angle;
	this->speed = (int)game->gameSpeed * MULTIPLIER_TWICE * PROJECTILE_SPEED;
	this->birthTime = (long int)SDL_GetTicks();
}

Projectile::~Projectile()
{
}

void Projectile::render()
{
	glPushMatrix();
	glColor3ub(200, 200, 200);
	glTranslated(posX, posY, posZ);
	glCallList(game->getObjectRenderer()->getSnowBall());
	glColor3ub(COLOR_MAX, COLOR_MAX, COLOR_MAX);
	glPopMatrix();
}

long int Projectile::getBirthTime()
{
	return birthTime;
}