#include "SnowRefill.h"

SnowRefill::SnowRefill(Game* game, int x, int y) :GameObject(SNOW_REFILL_RADIUS)
{
	this->game = game;
	this->posX = x;
	this->posY = y;
	this->posZ = ZERO;
	setSize();
	colorVariation = COLOR_MAX;
	colorDown = true;
	hoverValue = 5;
}

SnowRefill::~SnowRefill()
{
}

void SnowRefill::render()
{
	glPushMatrix();

	hoverValue += colorDown ? -SNOW_REFILL_HOVER_VALUE : SNOW_REFILL_HOVER_VALUE;
	glTranslated(posX, posY, hoverValue);

	if (colorDown && colorVariation > SNOW_REFILL_COLOR_MIN) {
		colorVariation--;
	}
	else if (!colorDown && colorVariation < SNOW_REFILL_COLOR_MAX) {
		colorVariation++;
	}
	else if (colorVariation <= SNOW_REFILL_COLOR_MIN || colorVariation >= SNOW_REFILL_COLOR_MAX) {
		colorDown = !colorDown;
	}
	glColor3ub(colorVariation, SNOW_REFILL_BASE_COLOR, colorVariation);

	glCallList(game->getObjectRenderer()->getSnowRefill());
	glColor3ub(COLOR_MAX, COLOR_MAX, COLOR_MAX);
	glPopMatrix();
}