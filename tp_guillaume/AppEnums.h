#ifndef APPENUMS_H
#define APPENUMS_H

// Directions used to move the objects
enum EDirections {
	LEFT,
	RIGHT,
	FORWARD,
	BACKWARD
};

// Scenes of the game
enum EScenes {
	S_GAME,
	S_MANUAL,
	S_HOME,
	S_OPTIONS
};

// Menu options
enum EMenu {
	M_START,
	M_MANUAL,
	M_OPTIONS,
	M_EXIT
};

enum ESnowManActions {
	SM_FORWARD,
	SM_TURN_LEFT,
	SM_TURN_RIGHT,
	SM_WAIT
};

enum EObjects {
	O_ALL,
	O_PLAYER,
	O_SNOWMEN,
	O_TREE
};

enum EButtonStatus {
	B_HOVER,
	B_PRESSED,
	B_NONE
};

enum EGameSpeed {
	GS_NORMAL = 1,
	GS_FAST,
	GS_FASTER,
	GS_VERY_FAST
};

// Constructor
class AppEnums
{
};

#endif // !1
