#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"
#include "sdlglutils.h"
class Game;

class TextureManager
{
private:
	// Parameters
	typedef std::map<std::string, GLuint> Textures;
	Textures textures;
public:
	// Constructor
	TextureManager();

	// Destructor
	~TextureManager();

	// Getter
	GLuint getTexture(std::string fileName);

	// Methods
	void addTexture(std::string fileName);
	void deleteTexture(std::string fileName);
	void setTextures();
};

#endif // !GAME_H