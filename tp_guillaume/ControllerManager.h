#ifndef CONTROLLERMANAGER_H
#define CONTROLLERMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"
#include "Game.h"

class Game;

class ControllerManager
{
public:
	// Constructor
	ControllerManager(Game* game);

	// Destructor
	~ControllerManager();

	// Methods
	void initialize(int &result);
	void controlGame();

private:
	// Parameters
	Game* game;
	SDL_GameController* controller = NULL;
	SDL_Haptic* controllerHaptic = NULL;
};

#endif // !GAME_H
