#ifndef MOUSEMANAGER_H
#define MOUSEMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"
#include "Game.h"
#include "AppTexture.h"

class Game;
class AppTexture;

class MouseManager
{
public:
	// Constructor
	MouseManager(Game* game);

	// Destructor
	~MouseManager();

	// Getters
	int getMouseX();
	int getMouseY();

	// Methods
	void initialize();
	void controlGame();
	void controlMenu();
	void hideCursor(bool on);
	void updateMousePosition();
	bool checkIsInButton(AppTexture button);

private:
	// Constructor
	MouseManager();

	// Parameters
	Game* game;

	// Variables
	int mouseMotion;
	int mouseX, mouseY;

	// Methods
	void centerMouse();
};

#endif // !GAME_H
