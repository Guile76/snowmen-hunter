#include "PineTree.h"

PineTree::PineTree()
{
}

PineTree::PineTree(Game* game, double radius, double posX, double posY) :GameObject(radius)
{
	this->game = game;
	this->posX = posX;
	this->posY = posY;
	this->posZ = ZERO;
	setSize();
}

PineTree::~PineTree()
{
}

void PineTree::render()
{
	glPushMatrix();
	glScaled(radius, radius, radius);
	glTranslated(posX, posY, posZ);
	glCallList(game->getObjectRenderer()->getPineTree());
	glPopMatrix();
}