#include "EnemiesManager.h"

EnemiesManager::EnemiesManager()
{
}

EnemiesManager::EnemiesManager(Game * game)
{
	this->game = game;
	srand((unsigned int)time(NULL));
}

EnemiesManager::~EnemiesManager()
{
}

void EnemiesManager::generateEnemy()
{
	enemies.push_back(new SnowMan(Utils::getRandomInt(-POP_LIMIT + SNOWMAN_RADIUS, POP_LIMIT - SNOWMAN_RADIUS), Utils::getRandomInt(-POP_LIMIT + SNOWMAN_RADIUS, POP_LIMIT - SNOWMAN_RADIUS), game, SNOWMAN_RADIUS));
}

void EnemiesManager::generateRegularEnemy()
{
	if ((long int)SDL_GetTicks() > snowManGenerationTick + SNOWMAN_GENERATION_RYTHM) {
		generateEnemy();
		snowManGenerationTick = (long int)SDL_GetTicks();
	}
}

void EnemiesManager::generateStartingEnemies(int nb)
{
	for (int i = ZERO; i < nb; i++) {
		generateEnemy();
	}
}

void EnemiesManager::moveEnemies()
{
	for (int i = ZERO; i < (int)enemies.size(); i++) {
		if (!enemies[i]->isBerserk) {
			enemies[i]->isBerserk = enemies[i]->checkBerserk();
		}
		enemies[i]->move();
	}
}

void EnemiesManager::renderEnemies()
{
	for (int i = ZERO; i < (int)enemies.size(); i++) {
		enemies[i]->render();
	}
}

void EnemiesManager::reset()
{
	for (int i = ZERO; i < (int)enemies.size(); i++) {
		enemies[i]->~SnowMan();
		enemies.erase(enemies.begin() + i);
	}
}