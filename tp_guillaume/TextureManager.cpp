#include "TextureManager.h"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
	std::map<std::string, GLuint>::iterator itr;
	for (itr = textures.begin(); itr != textures.end(); ++itr) {
		glDeleteTextures(ONE, &itr->second);
		deleteTexture(itr->first);
	}
}

GLuint TextureManager::getTexture(std::string fileName)
{
	return textures[fileName];
}

void TextureManager::addTexture(std::string fileName)
{
	GLuint tempTexture = loadTexture(fileName.c_str());
	textures.insert(std::pair<std::string, GLuint>(fileName, tempTexture));
}

void TextureManager::deleteTexture(std::string fileName)
{
	textures.erase(fileName);
}

void TextureManager::setTextures()
{
	addTexture(SKYBOX_FLOOR);
	addTexture(SKYBOX_BACK);
	addTexture(SKYBOX_FORWARD);
	addTexture(SKYBOX_LEFT);
	addTexture(SKYBOX_RIGHT);
	addTexture(SKYBOX_UP);
}