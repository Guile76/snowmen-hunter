#ifndef GAME_H
#define GAME_H

#include "AppEnums.h"
#include "AppDefines.h"
#include "AppIncludes.h"
#include "Player.h"
#include "ObjectRenderer.h"
#include "Projectile.h"
#include "Utils.h"
#include "EnemiesManager.h"
#include "ControllerManager.h"
#include "MouseManager.h"
#include "TextureManager.h"
#include "TextsManager.h"
#include "AudioManager.h"
#include "KeyboardManager.h"
#include "SnowRefill.h"
#include "PineTree.h"
#include "ObjectsManager.h"

class Player;
class Projectile;
class EnemiesManager;
class ControllerManager;
class MouseManager;
class ObjectRenderer;
class KeyboardManager;
class TextsManager;
class SnowRefill;
class PineTree;
class ObjectsManager;

class Game
{
public:

	// Constructor
	Game();

	// Destructor
	virtual ~Game();

	// Variables
	bool isRunning, isLost, isPaused, isGameControllerActive, hasController;
	int result;
	EGameSpeed gameSpeed;
	long int pauseGap;

	// Methods
	int initialize();
	void execute();
	void freeMemory();
	void changeScene(EScenes newScene, int &result);
	void togglePause();
	void resetGame();

	// Getters
	ObjectRenderer* getObjectRenderer();
	Player* getPlayer();
	EnemiesManager* getEnemiesManager();
	ObjectsManager* getObjectsManager();
	SDL_Window* getGameWindow();
	SDL_Event getGameEvent();
	AudioManager* getAudioManager();
	TextureManager* getTextureManager();
	TextsManager* getTextsManager();
	EScenes getScene();
	const Uint8* getStates();
	MouseManager* getMouseManager();
	int getWindowWidth();
	int getWindowHeight();
	int getScore();

private:
	// Parameters
	SDL_Window* menu_window = NULL;
	SDL_Window* game_window = NULL;
	SDL_Renderer* renderer = NULL;
	SDL_Event game_event;
	SDL_GLContext game_context;
	const Uint8* states;
	TextsManager* textsManager = NULL;
	TextureManager* textureManager = NULL;
	ObjectRenderer* objectRenderer = NULL;
	ObjectsManager* objectsManager = NULL;

	Player* player = NULL;

	ControllerManager* controllerManager = NULL;
	MouseManager* mouseManager = NULL;
	KeyboardManager* keyboardManager = NULL;
	EnemiesManager* enemiesManager = NULL;
	AudioManager* audioManager = NULL;

	// Variables
	int windowHeight, windowWidth, score;
	long int start, end;

	EScenes scene;

	// Methods
	void initializeSDL(int &result);
	void initializeIMG(int &result);
	void initializeTTF(int &result);
	void initializeMixer(int &result);
	void createGameWindow(int &result);
	void createMenuWindowAndRenderer(int &result);
	void initializeOpenGL();

	void destroyMenuWindow();
	void destroyGameWindow();
	void destroyWindows();
	void destroyGameContext();
	void destroyRenderer();
	void destroyTextsManager();

	void manageEvent();
	void erase();
	void updateModel();
	void drawModel();
	void updateView();

	void checkEnemiesPlayerCollision();
	void checkEnemiesProjectilesCollision();
	void checkPlayerSnowRefillsCollision();
};

#endif // !GAME_H
