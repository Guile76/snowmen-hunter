#ifndef UTILS_H
#define UTILS_H

#include "AppIncludes.h"
#include "AppDefines.h"

class Utils
{
public:
	// Methods
	static int getRandomInt(int min, int max);
	static float getDotCross(int x1, int y1, int x2, int y2);
	static double getDistance(double pos01X, double pos01Y, double pos02X, double pos02Y);

	// Structure
	struct MyVector
	{
		double x;
		double y;
		double distance;

		void set(double x, double y, double distance) {
			this->x = x;
			this->y = y;
			this->distance = distance;
		}
	};

private:
	// Constructor
	Utils();

	// Destructor
	~Utils();
};

#endif // !UTILS_H
