#ifndef APPINCLUDES_H
#define APPINCLUDES_H

#ifdef _WIN32
#include <Windows.h>
#include <SDL.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <GL/freeglut.h>
#endif

#if (defined(linux) || defined(__linux) || defined(__linux__))
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#endif

#include <string>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>

#endif // APPINCLUDES_H