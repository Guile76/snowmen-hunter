#ifndef TEXTSMANAGER_H
#define TEXTSMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"
#include "AppTexture.h"
#include "AppEnums.h"
#include "Game.h"

class Game;

class TextsManager
{
public:
	// Constructor
	TextsManager(Game* game, SDL_Renderer* renderer, int windowHeight);

	// Destructor
	~TextsManager();

	// Parameters
	TTF_Font* font = NULL;

	// Variables
	int fontSize, score;

	// Getters
	AppTexture getButtonPlay();
	AppTexture getButtonManual();
	AppTexture getButtonExit();
	AppTexture getButtonOptions();
	AppTexture getButtonBack();
	AppTexture getButtonChooseControls();
	AppTexture getBestScoreTexture();
	AppTexture getButtonGameSpeed();
	int getBestScore();

	// Methods
	void initializeTextsTextures();
	void setFont(std::string path, int size);
	void setTextTexture(AppTexture* texture, std::string text, int size, std::string fontPath = FONT_MAIN, int red = COLOR_MAX, int green = COLOR_MAX, int blue = COLOR_MAX);
	void setTexts(std::string path, std::vector<AppTexture*>* texts, int size, std::string fontPath = FONT_MAIN, int red = COLOR_MAX, int green = COLOR_MAX, int blue = COLOR_MAX);
	void initializeBestScore(std::string path);
	void setBestScore(std::string path, int newBestScore);

	void renderMenu(int windowWidth, int windowHeight);
	void renderManual(int windowWidth, int windowHeight);
	void renderOptions(int windowWidth, int windowHeight);

	void print(int windowWidth, int windowHeight, int posX, int posY, std::string text, void* font, int red = COLOR_MAX, int green = COLOR_MAX, int blue = COLOR_MAX);
	void displayGameInfo(int windowWidth, int windowHeight);
	void drawPointer(int windowWidth, int windowHeight);
	void drawPauseText(int windowWidth, int windowHeight);
	void drawGameOverText(int windowWidth, int windowHeight);

private:
	// Parameters
	Game* game;
	SDL_Renderer* renderer;
	SDL_Color textColor;

	AppTexture bgTexture, titleTexture, gameOverTexture, resultTexture, bestScoreTexture;
	AppTexture buttonPlayTexture, buttonManualTexture, buttonExitTexture, buttonOptionsTexture, buttonBackTexture, buttonChooseControls, buttonGameSpeed;
	std::vector<AppTexture*> manualText;

	// Variables
	std::string textScore, chosenControls, displayedController, displayedGameSpeed;
	int bestScore;

	// Methods
	void renderBG(int windowWidth, int windowHeight);
	void renderButton(int fontSize, std::string text, AppTexture* button, EMenu position, float height, int windowWidth, int windowHeight, std::string fontPath = FONT_MAIN);
};

#endif // !TEXTSMANAGER_H
