#include "Game.h"

Game::Game()
{
	player = new Player(this, PLAYER_RADIUS);
	enemiesManager = new EnemiesManager(this);
	objectRenderer = new ObjectRenderer(this);
	controllerManager = new ControllerManager(this);
	mouseManager = new MouseManager(this);
	keyboardManager = new KeyboardManager(this);
	textureManager = new TextureManager();
	audioManager = new AudioManager();
	objectsManager = new ObjectsManager(this);
}

Game::~Game()
{
}

int Game::initialize()
{
	// Datas
	result = ZERO;
	windowWidth = WIDTH_SCREEN;
	windowHeight = HEIGHT_SCREEN;
	gameSpeed = GS_NORMAL;

	isRunning = true;

	// System
	srand((unsigned int)time(NULL));
	initializeSDL(result);
	initializeIMG(result);
	initializeTTF(result);

	initializeMixer(result);
	audioManager->initialize();

	changeScene(S_HOME, result);

	isGameControllerActive = false;
	controllerManager->initialize(result);
	mouseManager->initialize();

	return result;
}

void Game::execute()
{
	glClearColor(ZERO, ZERO, ZERO, ONE);

	while (isRunning) {
		start = SDL_GetTicks();
		SDL_PollEvent(&game_event);
		states = SDL_GetKeyboardState(NULL);

		manageEvent();
		updateModel();
		erase();
		updateView();

		end = SDL_GetTicks();
		if (end - start < SECOND / FPS) {
			SDL_Delay((SECOND / FPS) - (end - start));
		}
	}
}

void Game::freeMemory()
{
	destroyTextsManager();
	destroyRenderer();
	destroyGameContext();
	destroyWindows();

	audioManager->freeMemory();
	Mix_CloseAudio();

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

void Game::changeScene(EScenes newScene, int &result)
{
	scene = newScene;

	switch (newScene)
	{
	case S_GAME:
		resetGame();
		if (NULL != menu_window) {
			destroyMenuWindow();
		}
		if (NULL == game_window) {
			createGameWindow(result);
			initializeOpenGL();
			textureManager->setTextures();
			objectRenderer->initialize();

			objectsManager->generatePineTrees();
		}
		break;
	case S_MANUAL:
	case S_OPTIONS:
	case S_HOME:
		if (NULL != game_window) {
			destroyGameWindow();
		}
		if (NULL == menu_window) {
			createMenuWindowAndRenderer(result);
			destroyTextsManager();
			textsManager = new TextsManager(this, renderer, windowHeight);
			textsManager->initializeTextsTextures();
			textsManager->initializeBestScore(TEXT_BESTSCORE);
		}
		break;
	}
}

void Game::togglePause()
{
	if ((int)SDL_GetTicks() > pauseGap + PAUSE_PRESS_DELAY) {
		isPaused = !isPaused;
		pauseGap = (int)SDL_GetTicks();
	}
}

void Game::resetGame()
{
	isPaused = isLost = false;
	score = ZERO;

	player->reset();
	enemiesManager->reset();

	enemiesManager->generateStartingEnemies(SNOWMAN_STARTING_NUMBER);
	//enemiesManager->enemies.push_back(new SnowMan(150, 0, this, 20));
}

ObjectRenderer * Game::getObjectRenderer()
{
	return objectRenderer;
}

Player * Game::getPlayer()
{
	return player;
}

EnemiesManager * Game::getEnemiesManager()
{
	return enemiesManager;
}

ObjectsManager * Game::getObjectsManager()
{
	return objectsManager;
}

SDL_Window * Game::getGameWindow()
{
	return game_window;
}

SDL_Event Game::getGameEvent()
{
	return game_event;
}

AudioManager * Game::getAudioManager()
{
	return audioManager;
}

TextureManager * Game::getTextureManager()
{
	return textureManager;
}

TextsManager * Game::getTextsManager()
{
	return textsManager;
}

EScenes Game::getScene()
{
	return scene;
}

const Uint8 * Game::getStates()
{
	return states;
}

MouseManager * Game::getMouseManager()
{
	return mouseManager;
}

int Game::getWindowWidth()
{
	return windowWidth;
}

int Game::getWindowHeight()
{
	return windowHeight;
}

int Game::getScore()
{
	return score;
}

void Game::initializeSDL(int & result)
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC | SDL_INIT_EVENTS | SDL_INIT_AUDIO) < ZERO)
	{
		SDL_Log("SDL initialization failed. SDL Error : %s", SDL_GetError());
		result = EXIT_FAILURE;
	}
}

void Game::initializeIMG(int & result)
{
	if (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) < ZERO) {
		SDL_Log("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
		result = EXIT_FAILURE;
	}
}

void Game::initializeTTF(int & result)
{
	if (TTF_Init() < ZERO) {
		SDL_Log("SDL_TTF could not initialize! SDL_TTF Error: %s\n", TTF_GetError());
		result = EXIT_FAILURE;
	}
}

void Game::initializeMixer(int & result)
{
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < ZERO) {
		SDL_Log("SDL_mixer could not initialize ! SDL_mixer Error: %s\n", Mix_GetError());
		result = EXIT_FAILURE;
	}
}

void Game::createMenuWindowAndRenderer(int & result)
{
	if (NULL != renderer) {
		destroyRenderer();
	}
	if (NULL != menu_window) {
		destroyMenuWindow();
	}

	if (SDL_CreateWindowAndRenderer(WIDTH_SCREEN, HEIGHT_SCREEN, SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_RENDERER_PRESENTVSYNC, &menu_window, &renderer) < ZERO) {
		SDL_Log("Couldn't create window and renderer. SDL Error : %s", SDL_GetError());
		result = EXIT_FAILURE;
	}
	else {
		SDL_SetWindowTitle(menu_window, GAME_TITLE);
		// Gets the display size
		SDL_GetWindowSize(menu_window, &windowWidth, &windowHeight);
	}
}

void Game::createGameWindow(int & result)
{
	windowWidth = GetSystemMetrics(SM_CXSCREEN);
	windowHeight = GetSystemMetrics(SM_CYSCREEN);
	game_window = SDL_CreateWindow(GAME_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	result = game_window != NULL ? EXIT_SUCCESS : EXIT_FAILURE;
}

void Game::initializeOpenGL()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	if (NULL != game_context) {
		game_context = NULL;
	}
	game_context = SDL_GL_CreateContext(game_window);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70, (double)(windowWidth * WINDOW_RATIO_MODIFIER / windowHeight), 1, 40000);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	SDL_GL_SetSwapInterval(ONE);
}

void Game::destroyMenuWindow()
{
	SDL_DestroyWindow(menu_window);
	menu_window = NULL;
}

void Game::destroyGameWindow()
{
	SDL_DestroyWindow(game_window);
	game_window = NULL;
}

void Game::destroyWindows()
{
	if (NULL != game_window) {
		destroyGameWindow();
	}
	if (NULL == menu_window) {
		destroyMenuWindow();
	}
}

void Game::destroyGameContext()
{
	SDL_GL_DeleteContext(game_context);
	game_context = NULL;
}

void Game::destroyRenderer()
{
	SDL_DestroyRenderer(renderer);
	renderer = NULL;
}

void Game::destroyTextsManager()
{
	if (NULL != textsManager) {
		TTF_CloseFont(textsManager->font);
		textsManager->font = NULL;
		delete(textsManager);
		textsManager = NULL;
	}
}

void Game::manageEvent()
{
	switch (scene)
	{
	case S_GAME:

		if (!isGameControllerActive) {
			keyboardManager->controlGame();
			mouseManager->controlGame();
		}
		else {
			controllerManager->controlGame();
		}

		if (!isLost && !isPaused) {
			// Snowmen generation
			enemiesManager->generateRegularEnemy();
			// Random Refills generation
			objectsManager->generateRandomRefill();

			checkEnemiesPlayerCollision();
			checkEnemiesProjectilesCollision();
			checkPlayerSnowRefillsCollision();
		}

		break;
	case S_MANUAL:
	case S_OPTIONS:
	case S_HOME:
		mouseManager->controlMenu();
		break;
	}
}

void Game::erase()
{
	switch (scene)
	{
	case S_GAME:
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		break;
	case S_MANUAL:
	case S_OPTIONS:
	case S_HOME:
		SDL_SetRenderDrawColor(renderer, ZERO, ZERO, ZERO, COLOR_MAX);
		SDL_RenderClear(renderer);
		break;
	}
}

void Game::updateModel()
{
	switch (scene)
	{
	case S_GAME:
		if (!isLost && !isPaused) {
			enemiesManager->moveEnemies();
			player->shot();
			player->setProjectiles();
		}
		break;
	case S_MANUAL:
	case S_OPTIONS:
	case S_HOME:
		mouseManager->updateMousePosition();
		break;
	}
}

void Game::drawModel()
{
	mouseManager->hideCursor(S_GAME == scene ? true : false);

	switch (scene)
	{
	case S_GAME:

		glCallList(objectRenderer->getSkybox());

		if (!isLost && !isPaused) {
			enemiesManager->renderEnemies();
			player->renderProjectiles();
			objectsManager->renderRefills();
		}

		objectsManager->renderPineTrees();
		player->render();

		break;
	case S_MANUAL:
		textsManager->renderManual(windowWidth, windowHeight);
		break;
	case S_OPTIONS:
		textsManager->renderOptions(windowWidth, windowHeight);
		break;
	case S_HOME:
		textsManager->renderMenu(windowWidth, windowHeight);
		break;
	}
}

void Game::updateView()
{
	switch (scene)
	{
	case S_GAME:
		// Main viewport
		glViewport(ZERO, ZERO, windowWidth, windowHeight);
		player->setGameView();
		drawModel();
		if (!isLost && !isPaused) {
			textsManager->drawPointer(windowWidth, windowHeight);
		}
		else if (isPaused) {
			textsManager->drawPauseText(windowWidth, windowHeight);
		}
		else {
			textsManager->drawGameOverText(windowWidth, windowHeight);
		}

		// Map viewport
		glViewport((GLint)(windowWidth * MULTIPLIER_FIFTH * MULTIPLIER_FOUR_TIME), (GLint)(windowHeight * MULTIPLIER_QUARTER * MULTIPLIER_THRICE), (GLsizei)(windowWidth * MULTIPLIER_FIFTH), (GLsizei)(windowHeight * MULTIPLIER_QUARTER));
		player->setGameCamera();
		drawModel();
		textsManager->displayGameInfo(windowWidth, windowHeight);

		// Draw complete scene
		glFlush();
		SDL_GL_SwapWindow(game_window);
		break;
	case S_MANUAL:
	case S_OPTIONS:
	case S_HOME:
		drawModel();
		SDL_RenderPresent(renderer);
		break;
	}
}

void Game::checkEnemiesPlayerCollision()
{
	for (int i = ZERO; i < (int)enemiesManager->enemies.size(); i++) {
		if (SDL_HasIntersection(enemiesManager->enemies[i]->getBounds(), player->getBounds())) {
			enemiesManager->enemies[i]->~SnowMan();
			enemiesManager->enemies.erase(enemiesManager->enemies.begin() + i);
			audioManager->playSoundOuch();
			if (player->removeLifeAndCheckDeath()) {
				player->isAlive = false;
				isLost = true;
				if (score > textsManager->getBestScore()) {
					audioManager->playSoundSuccess();
				}
				else {
					audioManager->playSoundFailure();
				}
			}
		}
	}
}

void Game::checkEnemiesProjectilesCollision()
{
	for (int i = ZERO; i < (int)player->playerProjectiles.size(); i++) {
		for (int j = ZERO; j < (int)enemiesManager->enemies.size(); j++) {
			if (player->playerProjectiles[i]->isActive && SDL_HasIntersection(enemiesManager->enemies[j]->getBounds(), player->playerProjectiles[i]->getBounds())) {
				player->playerProjectiles[i]->isActive = false;
				if (enemiesManager->enemies[j]->removeHPAndCheckDeath()) {
					enemiesManager->enemies.erase(enemiesManager->enemies.begin() + j);
					score++;
					audioManager->playSoundSnowmanDeath();
				}
				else {
					audioManager->playSoundSnowmanHit();
				}
			}
		}
	}
}

void Game::checkPlayerSnowRefillsCollision()
{
	for (int i = ZERO; i < (int)objectsManager->snowRefills.size(); i++) {
		if (SDL_HasIntersection(objectsManager->snowRefills[i]->getBounds(), player->getBounds())) {
			objectsManager->snowRefills[i]->~SnowRefill();
			objectsManager->snowRefills.erase(objectsManager->snowRefills.begin() + i);
			player->increaseSnowPool(SNOW_REFILL_AMOUNT);
			audioManager->playSoundPowerUp();
		}
	}
}