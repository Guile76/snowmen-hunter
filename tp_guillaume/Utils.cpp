#include "Utils.h"

int Utils::getRandomInt(int min, int max)
{
	return min + (rand() % static_cast<int>(max - min + ONE));
}

float Utils::getDotCross(int x1, int y1, int x2, int y2)
{
	return (float)(x1 * y2) - (float)(y1 * x2);
}

double Utils::getDistance(double pos01X, double pos01Y, double pos02X, double pos02Y)
{
	return sqrt((pos02X - pos01X) * (pos02X - pos01X) + (pos02Y - pos01Y) * (pos02Y - pos01Y));
}

Utils::Utils()
{
}

Utils::~Utils()
{
}