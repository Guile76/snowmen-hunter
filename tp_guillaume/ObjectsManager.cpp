#include "ObjectsManager.h"

ObjectsManager::ObjectsManager(Game* game)
{
	this->game = game;
	srand((unsigned int)time(NULL));
	nextRandomRefill = Utils::getRandomInt(MIN_REFILL_POP_DELAY, MAX_REFILL_POP_DELAY);
	lastRandomRefill = ZERO;
}

ObjectsManager::~ObjectsManager()
{
}

void ObjectsManager::generateRefills(int posX, int posY)
{
	snowRefills.push_back(new SnowRefill(this->game, posX, posY));
}

void ObjectsManager::generateRandomRefill()
{
	if ((int)SDL_GetTicks() >= lastRandomRefill + nextRandomRefill) {
		generateRefills(Utils::getRandomInt(-POP_LIMIT, POP_LIMIT), Utils::getRandomInt(-POP_LIMIT, POP_LIMIT));
		lastRandomRefill = SDL_GetTicks();
		nextRandomRefill = Utils::getRandomInt(MIN_REFILL_POP_DELAY, MAX_REFILL_POP_DELAY);
	}
}

void ObjectsManager::generatePineTree()
{
	int radius = Utils::getRandomInt(TREE_MIN_RADIUS, TREE_MAX_RADIUS);
	pineTrees.push_back(new PineTree(this->game, radius, Utils::getRandomInt(-POP_LIMIT + radius, POP_LIMIT - radius), Utils::getRandomInt(-POP_LIMIT + radius, POP_LIMIT - radius)));
}

void ObjectsManager::generatePineTrees()
{
	int nbPineTrees = Utils::getRandomInt(MIN_TREES, MAX_TREES);
	for (int i = ZERO; i < nbPineTrees; i++) {
		generatePineTree();
	}
}

void ObjectsManager::renderRefills()
{
	for (int i = ZERO; i < (int)snowRefills.size(); i++) {
		snowRefills[i]->render();
	}
}

void ObjectsManager::renderPineTrees()
{
	for (int i = ZERO; i < (int)pineTrees.size(); i++) {
		pineTrees[i]->render();
	}
}

void ObjectsManager::reset()
{
	for (int i = ZERO; i < (int)snowRefills.size(); i++) {
		snowRefills[i]->~SnowRefill();
		snowRefills.erase(snowRefills.begin() + i);
	}

	for (int i = ZERO; i < (int)pineTrees.size(); i++) {
		pineTrees[i]->~PineTree();
		pineTrees.erase(pineTrees.begin() + i);
	}
}