#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "GameObject.h"
#include "Game.h"
#include "AppDefines.h"
class Game;

class Projectile :
	public GameObject
{
public:
	// Constructor
	Projectile(double x, double y, double z, double angle, Game* game, double radius);

	// Destructor
	~Projectile();

	// Methods
	void render() override;

	// Getter
	long int getBirthTime();

private:
	// Constructor
	Projectile();

	// Parameters
	Game* game;

	// Variables
	long int birthTime;
};

#endif // !PLAYER_H
