#include "KeyboardManager.h"

KeyboardManager::KeyboardManager(Game* game)
{
	this->game = game;
}

KeyboardManager::~KeyboardManager()
{
}

void KeyboardManager::controlGame()
{
	if (game->getPlayer()->isAlive && !game->isPaused) {
		if (game->getStates()[SDL_SCANCODE_W]) {
			game->getPlayer()->authMoveForward();
		}
		if (game->getStates()[SDL_SCANCODE_S]) {
			game->getPlayer()->authMoveBackward();
		}
		if (game->getStates()[SDL_SCANCODE_A]) {
			game->getPlayer()->authMoveLeft();
		}
		if (game->getStates()[SDL_SCANCODE_D]) {
			game->getPlayer()->authMoveRight();
		}
		if (game->getStates()[SDL_SCANCODE_LSHIFT]) {
			game->getPlayer()->run();
		}
		else {
			game->getPlayer()->walk();
		}
	}

	if ((game->isPaused || game->isLost) && game->getStates()[SDL_SCANCODE_ESCAPE]) {
		game->changeScene(S_HOME, game->result);
	}

	if (game->getStates()[SDL_SCANCODE_SPACE]) {
		if (!game->isLost) {
			game->togglePause();
		}
		else {
			game->changeScene(S_GAME, game->result);
			game->pauseGap = SDL_GetTicks();
		}
	}
}