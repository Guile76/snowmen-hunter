#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include "Game.h"
#include "Projectile.h"
#include "AppDefines.h"

class Game;
class Projectile;

class Player : public GameObject
{
public:
	// Constructor
	Player(Game* game, double radius);

	// Destructor
	~Player();

	// Parameter
	std::vector<Projectile*> playerProjectiles;

	// Variables
	bool isAlive, isAllowedToShot, isShooting, isRunning;

	// Methods
	void setGameView();
	void setGameCamera();
	void render() override;

	void shot();
	void setProjectiles();
	void renderProjectiles();

	void authMoveForward();
	void authMoveBackward();
	void authMoveLeft();
	void authMoveRight();
	void setAngle(int angle);

	void walk();
	void run();

	void addLife();
	bool removeLifeAndCheckDeath();

	void increaseSnowPool(int value);
	bool checkAndReduceSnowPool(int value);

	void reset();

	// Getters
	int getLives();
	int getSnowPool();

private:
	// Constructor
	Player();

	// Parameters
	Game* game;

	// Variables
	double targetHeight, viewHeight;
	long int shootDelayTick;
	int canonMultiplier, lives, snowPool;

	// Methods
	bool checkAboutToCollide(EDirections direction);
};

#endif // !PLAYER_H
