#include "ObjectRenderer.h"

ObjectRenderer::ObjectRenderer(Game* game)
{
	this->game = game;
}

ObjectRenderer::~ObjectRenderer()
{
	glDeleteLists(idSkybox, ONE);
	glDeleteLists(idSnowMan, ONE);
	glDeleteLists(idSnowBall, ONE);
	glDeleteLists(idCylinder, ONE);
	glDeleteLists(idCone, ONE);
	glDeleteLists(idDisk, ONE);
	glDeleteLists(idCarott, ONE);
	glDeleteLists(idBall, ONE);
	glDeleteLists(idCanonRight, ONE);
	glDeleteLists(idCanonLeft, ONE);
	glDeleteLists(idTank, ONE);
	glDeleteLists(idSnowRefill, ONE);
	glDeleteLists(idPineTree, ONE);
}

void ObjectRenderer::initialize()
{
	initializeSkybox();
	initializeSnowBall();
	initializeCylinder();
	initializeCone();
	initializeDisk();
	initializeCarott();
	initializeBall();
	initializeSnowMan();
	initializeCanonRight();
	initializeCanonLeft();
	initializeTank();
	initializeSnowRefill();
	initializePineTree();
}

GLuint ObjectRenderer::getSkybox()
{
	return idSkybox;
}

GLuint ObjectRenderer::getSnowBall()
{
	return idSnowBall;
}

GLuint ObjectRenderer::getCylinder()
{
	return idCylinder;
}

GLuint ObjectRenderer::getCone()
{
	return idCone;
}

GLuint ObjectRenderer::getDisk()
{
	return idDisk;
}

GLuint ObjectRenderer::getCarott()
{
	return idCarott;
}

GLuint ObjectRenderer::getBall()
{
	return idBall;
}

GLuint ObjectRenderer::getSnowMan()
{
	return idSnowMan;
}

GLuint ObjectRenderer::getCanonRight()
{
	return idCanonRight;
}

GLuint ObjectRenderer::getCanonLeft()
{
	return idCanonLeft;
}

GLuint ObjectRenderer::getTank()
{
	return idTank;
}

GLuint ObjectRenderer::getSnowRefill()
{
	return idSnowRefill;
}

GLuint ObjectRenderer::getPineTree()
{
	return idPineTree;
}

void ObjectRenderer::initializeSkybox()
{
	idSkybox = glGenLists(ONE);
	glNewList(idSkybox, GL_COMPILE);
	glPushMatrix();
	glColor3ub(COLOR_MAX, COLOR_MAX, COLOR_MAX);

	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_FLOOR));
	glBegin(GL_QUADS);
	glTexCoord2d(WORLD_FLOOR_REPEAT, ZERO);
	glVertex3i(-WORLD_RADIUS, -WORLD_RADIUS, ZERO);
	glTexCoord2d(WORLD_FLOOR_REPEAT, WORLD_FLOOR_REPEAT);
	glVertex3i(-WORLD_RADIUS, WORLD_RADIUS, ZERO);
	glTexCoord2d(ZERO, WORLD_FLOOR_REPEAT);
	glVertex3i(WORLD_RADIUS, WORLD_RADIUS, ZERO);
	glTexCoord2d(ZERO, ZERO);
	glVertex3i(WORLD_RADIUS, -WORLD_RADIUS, ZERO);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_RIGHT));
	glBegin(GL_QUADS);
	glTexCoord2d(ONE, ZERO);
	glVertex3i(WORLD_RADIUS, -WORLD_RADIUS, WORLD_DEPTH);
	glTexCoord2d(ONE, ONE);
	glVertex3i(WORLD_RADIUS, -WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ZERO, ONE);
	glVertex3i(WORLD_RADIUS, WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ZERO, ZERO);
	glVertex3i(WORLD_RADIUS, WORLD_RADIUS, WORLD_DEPTH);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_FORWARD));
	glBegin(GL_QUADS);
	glTexCoord2d(ZERO, ZERO);
	glVertex3i(WORLD_RADIUS, -WORLD_RADIUS, WORLD_DEPTH);
	glTexCoord2d(ZERO, ONE);
	glVertex3i(WORLD_RADIUS, -WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ONE, ONE);
	glVertex3i(-WORLD_RADIUS, -WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ONE, ZERO);
	glVertex3i(-WORLD_RADIUS, -WORLD_RADIUS, WORLD_DEPTH);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_LEFT));
	glBegin(GL_QUADS);
	glTexCoord2d(ZERO, ZERO);
	glVertex3i(-WORLD_RADIUS, -WORLD_RADIUS, WORLD_DEPTH);
	glTexCoord2d(ZERO, ONE);
	glVertex3i(-WORLD_RADIUS, -WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ONE, ONE);
	glVertex3i(-WORLD_RADIUS, WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ONE, ZERO);
	glVertex3i(-WORLD_RADIUS, WORLD_RADIUS, WORLD_DEPTH);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_BACK));
	glBegin(GL_QUADS);
	glTexCoord2d(ONE, ZERO);
	glVertex3i(WORLD_RADIUS, WORLD_RADIUS, WORLD_DEPTH);
	glTexCoord2d(ONE, ONE);
	glVertex3i(WORLD_RADIUS, WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ZERO, ONE);
	glVertex3i(-WORLD_RADIUS, WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ZERO, ZERO);
	glVertex3i(-WORLD_RADIUS, WORLD_RADIUS, WORLD_DEPTH);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_UP));
	glBegin(GL_QUADS);
	glTexCoord2d(ONE, ONE);
	glVertex3i(-WORLD_RADIUS, -WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ZERO, ONE);
	glVertex3i(-WORLD_RADIUS, WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ZERO, ZERO);
	glVertex3i(WORLD_RADIUS, WORLD_RADIUS, WORLD_HEIGHT);
	glTexCoord2d(ONE, ZERO);
	glVertex3i(WORLD_RADIUS, -WORLD_RADIUS, WORLD_HEIGHT);
	glEnd();

	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeSnowBall()
{
	idSnowBall = glGenLists(ONE);
	glNewList(idSnowBall, GL_COMPILE);
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_FLOOR));
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluQuadricTexture(q_params, GL_TRUE);
	gluSphere(q_params, ONE, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeCylinder()
{
	idCylinder = glGenLists(ONE);
	glNewList(idCylinder, GL_COMPILE);
	glPushMatrix();
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluCylinder(q_params, ONE, ONE, ONE, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeCone()
{
	idCone = glGenLists(ONE);
	glNewList(idCone, GL_COMPILE);
	glPushMatrix();
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluCylinder(q_params, ONE, ZERO, ONE, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeDisk()
{
	idDisk = glGenLists(ONE);
	glNewList(idDisk, GL_COMPILE);
	glPushMatrix();
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluDisk(q_params, ZERO, ONE, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeCarott()
{
	idCarott = glGenLists(ONE);
	glNewList(idCarott, GL_COMPILE);
	glPushMatrix();
	glRotated(RIGHT_ANGLE, ZERO, ONE, ZERO);
	glColor3ub(COLOR_MAX, WORLD_FLOOR_REPEAT, ZERO);
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluCylinder(q_params, ONE, ZERO, ONE, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeBall()
{
	idBall = glGenLists(ONE);
	glNewList(idBall, GL_COMPILE);
	glPushMatrix();
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluSphere(q_params, ONE, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeSnowMan()
{
	idSnowMan = glGenLists(ONE);
	glNewList(idSnowMan, GL_COMPILE);

	// First body ball
	glPushMatrix();
	glTranslated(ZERO, ZERO, 10);
	glScaled(20, 20, 20);
	glCallList(idSnowBall);
	glPopMatrix();

	// Second body ball
	glPushMatrix();
	glTranslated(ZERO, ZERO, 30);
	glScaled(14, 14, 14);
	glCallList(idSnowBall);
	glPopMatrix();

	// Head
	glPushMatrix();
	glTranslated(ZERO, ZERO, 45);
	glScaled(8, 8, 8);
	glCallList(idSnowBall);
	glPopMatrix();

	// Hat
	glPushMatrix();
	glColor3ub(ZERO, ZERO, ZERO);
	// Bottom of the hat edge
	glPushMatrix();
	glTranslated(ZERO, ZERO, 50);
	glPushMatrix();
	glScaled(10, 10, ONE);
	glCallList(idDisk);
	glPopMatrix();

	// Hat edge
	glPushMatrix();
	glScaled(10, 10, ONE);
	glCallList(idCylinder);
	glPopMatrix();
	glTranslated(ZERO, ZERO, ONE);
	// Top of the hat edge
	glPushMatrix();
	glScaled(10, 10, ONE);
	glCallList(idCylinder);
	glPopMatrix();

	// Hat body
	glPushMatrix();
	glScaled(7, 7, 10);
	glCallList(idCylinder);
	glPopMatrix();
	// Top of the hat body
	glTranslated(ZERO, ZERO, 10);
	glScaled(7, 7, 10);
	glCallList(idDisk);
	glPopMatrix();

	// Eyes
	glPushMatrix();
	glTranslated(7, 2, 48);
	glCallList(idBall);
	glTranslated(ZERO, -4, ZERO);
	glCallList(idBall);
	glPopMatrix();
	glPopMatrix();

	// Nose
	glPushMatrix();
	glTranslated(7.5, ZERO, 45);
	glScaled(8, 2, 2);
	glCallList(idCarott);
	glPopMatrix();

	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializeCanonRight()
{
	idCanonRight = glGenLists(ONE);
	glNewList(idCanonRight, GL_COMPILE);

	glPushMatrix();
	// Color of the canons
	glColor3ub(200, 200, 200);

	glPushMatrix();
	glTranslated(-CANON_RECOIL, -4, -3);
	glRotated(RIGHT_ANGLE, ZERO, ONE, ZERO);
	glScaled(1.1, 1.1, 10);
	glCallList(idCylinder);
	glPopMatrix();

	glPushMatrix();
	glTranslated(ZERO, 4, -3);
	glRotated(RIGHT_ANGLE, ZERO, ONE, ZERO);
	glScaled(1.1, 1.1, 10);
	glCallList(idCylinder);
	glPopMatrix();
	glPopMatrix();

	glEndList();
}

void ObjectRenderer::initializeCanonLeft()
{
	idCanonLeft = glGenLists(ONE);
	glNewList(idCanonLeft, GL_COMPILE);

	glPushMatrix();
	// Color of the canons
	glColor3ub(200, 200, 200);

	glPushMatrix();
	glTranslated(ZERO, -4, -3);
	glRotated(RIGHT_ANGLE, ZERO, ONE, ZERO);
	glScaled(1.1, 1.1, 10);
	glCallList(idCylinder);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-CANON_RECOIL, 4, -3);
	glRotated(RIGHT_ANGLE, ZERO, ONE, ZERO);
	glScaled(1.1, 1.1, 10);
	glCallList(idCylinder);
	glPopMatrix();
	glPopMatrix();

	glEndList();
}

void ObjectRenderer::initializeTank()
{
	idTank = glGenLists(ONE);
	glNewList(idTank, GL_COMPILE);

	// Tank body
	glPushMatrix();
	glColor3ub(200, 225, 200);
	glTranslated(-9.5, ZERO, ZERO);
	glBegin(GL_QUADS);
	glVertex3i(-TANK_BODY_SIZE, -TANK_BODY_SIZE, ZERO);
	glVertex3i(-TANK_BODY_SIZE, TANK_BODY_SIZE, ZERO);
	glVertex3i(TANK_BODY_SIZE, TANK_BODY_SIZE, ZERO);
	glVertex3i(TANK_BODY_SIZE, -TANK_BODY_SIZE, ZERO);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3i(TANK_BODY_SIZE, -TANK_BODY_SIZE, ZERO);
	glVertex3i(TANK_BODY_SIZE, -TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(TANK_BODY_SIZE, TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(TANK_BODY_SIZE, TANK_BODY_SIZE, ZERO);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3i(TANK_BODY_SIZE, -TANK_BODY_SIZE, ZERO);
	glVertex3i(TANK_BODY_SIZE, -TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, -TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, -TANK_BODY_SIZE, ZERO);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3i(-TANK_BODY_SIZE, -TANK_BODY_SIZE, ZERO);
	glVertex3i(-TANK_BODY_SIZE, -TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, TANK_BODY_SIZE, ZERO);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3i(TANK_BODY_SIZE, TANK_BODY_SIZE, ZERO);
	glVertex3i(TANK_BODY_SIZE, TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, TANK_BODY_SIZE, ZERO);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3i(-TANK_BODY_SIZE, -TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(-TANK_BODY_SIZE, TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(TANK_BODY_SIZE, TANK_BODY_SIZE, TANK_BODY_SIZE);
	glVertex3i(TANK_BODY_SIZE, -TANK_BODY_SIZE, TANK_BODY_SIZE);
	glEnd();
	glPopMatrix();

	glEndList();
}

void ObjectRenderer::initializeSnowRefill()
{
	idSnowRefill = glGenLists(ONE);
	glNewList(idSnowRefill, GL_COMPILE);
	glPushMatrix();
	glTranslated(ZERO, ZERO, 10);
	glBindTexture(GL_TEXTURE_2D, game->getTextureManager()->getTexture(SKYBOX_FLOOR));
	q_params = gluNewQuadric();
	gluQuadricDrawStyle(q_params, GLU_FILL);
	gluQuadricTexture(q_params, GL_TRUE);
	gluSphere(q_params, SNOW_REFILL_RADIUS, 10, 10);
	glPopMatrix();
	glEndList();
}

void ObjectRenderer::initializePineTree()
{
	idPineTree = glGenLists(ONE);
	glNewList(idPineTree, GL_COMPILE);
	glPushMatrix();

	glPushMatrix();
	glScaled(ONE, ONE, 10);
	glColor3ub(160, 130, 80);
	glCallList(idCylinder);
	glPopMatrix();

	glColor3ub(ZERO, 100, ZERO);

	glPushMatrix();
	glTranslated(ZERO, ZERO, 10);
	glScaled(5, 5, 20);
	glCallList(idCone);
	glPopMatrix();

	glPushMatrix();
	glTranslated(ZERO, ZERO, 20);
	glScaled(4, 4, 20);
	glCallList(idCone);
	glPopMatrix();

	glPushMatrix();
	glTranslated(ZERO, ZERO, 30);
	glScaled(3, 3, 20);
	glCallList(idCone);
	glPopMatrix();

	glPushMatrix();
	glTranslated(ZERO, ZERO, 40);
	glScaled(2, 2, 20);
	glCallList(idCone);
	glPopMatrix();

	glColor3ub(COLOR_MAX, COLOR_MAX, COLOR_MAX);
	glPopMatrix();
	glEndList();
}