#ifndef OBJECTSMANAGER_H
#define OBJECTSMANAGER_H

#include "AppIncludes.h"
#include "SnowRefill.h"
#include "PineTree.h"
#include "Game.h"

class SnowRefill;
class PineTree;
class Game;

class ObjectsManager
{
public:
	// Constructor
	ObjectsManager(Game* game);

	// Destructor
	~ObjectsManager();

	// Methods
	void generateRefills(int posX, int posY);
	void generateRandomRefill();
	void generatePineTree();
	void generatePineTrees();

	void renderRefills();
	void renderPineTrees();

	void reset();

	// Parameters
	std::vector<SnowRefill*> snowRefills;
	std::vector<PineTree*> pineTrees;

private:
	// Parameters
	Game* game;

	// Variables
	long int lastRandomRefill, nextRandomRefill;
};

#endif // !OBJECTSMANAGER_H
