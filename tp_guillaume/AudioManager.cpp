#include "AudioManager.h"

AudioManager::AudioManager()
{
	channelEffectPlayerWalk = channelEffectPlayerShoot = channelEffectPlayerRun = channelEffectSnowmanHit = channelEffectSnowmanBerserk = channelEffectSnowmanDeath = channelEffectOuch = channelEffectSuccess = channelEffectFailure = channelEffectPowerUp = -ONE;
}

AudioManager::~AudioManager()
{
}

void AudioManager::initialize()
{
	soundEffectPlayerWalk = Mix_LoadWAV(SOUND_EFFECT_PLAYER_WALK);
	if (soundEffectPlayerWalk == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectPlayerShoot = Mix_LoadWAV(SOUND_EFFECT_PLAYER_SHOOT);
	if (soundEffectPlayerShoot == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectPlayerRun = Mix_LoadWAV(SOUND_EFFECT_PLAYER_RUN);
	if (soundEffectPlayerRun == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectSnowmanHit = Mix_LoadWAV(SOUND_EFFECT_SNOWMAN_HIT);
	if (soundEffectSnowmanHit == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectSnowmanBerserk = Mix_LoadWAV(SOUND_EFFECT_SNOWMAN_BERSERK);
	if (soundEffectSnowmanBerserk == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectSnowmanDeath = Mix_LoadWAV(SOUND_EFFECT_SNOWMAN_DEATH);
	if (soundEffectSnowmanDeath == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectOuch = Mix_LoadWAV(SOUND_EFFECT_LIFE_LOST);
	if (soundEffectOuch == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectSuccess = Mix_LoadWAV(SOUND_EFFECT_SUCCESS);
	if (soundEffectSuccess == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectFailure = Mix_LoadWAV(SOUND_EFFECT_FAILURE);
	if (soundEffectFailure == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}
	soundEffectPowerUp = Mix_LoadWAV(SOUND_EFFECT_POWER_UP);
	if (soundEffectPowerUp == NULL) {
		SDL_Log("Failed to load scratch sound effect! SDL_mixer Error: %s", Mix_GetError());
	}

	Mix_VolumeMusic(MIX_MAX_VOLUME);
	Mix_VolumeChunk(soundEffectPlayerWalk, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectPlayerShoot, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectPlayerRun, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectSnowmanHit, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectSnowmanBerserk, MIX_MAX_VOLUME);
	Mix_VolumeChunk(soundEffectSnowmanDeath, MIX_MAX_VOLUME);
	Mix_VolumeChunk(soundEffectOuch, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectSuccess, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectFailure, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));
	Mix_VolumeChunk(soundEffectPowerUp, (int)(MIX_MAX_VOLUME * MULTIPLIER_HALF));

	changeMusic(MUSIC_MAIN);
}

void AudioManager::freeMemory()
{
	Mix_FreeMusic(musicGame);
	musicGame = NULL;
	Mix_FreeChunk(soundEffectPlayerWalk);
	soundEffectPlayerWalk = NULL;
	Mix_FreeChunk(soundEffectPlayerShoot);
	soundEffectPlayerShoot = NULL;
	Mix_FreeChunk(soundEffectPlayerRun);
	soundEffectPlayerRun = NULL;
	Mix_FreeChunk(soundEffectSnowmanHit);
	soundEffectSnowmanHit = NULL;
	Mix_FreeChunk(soundEffectSnowmanBerserk);
	soundEffectSnowmanBerserk = NULL;
	Mix_FreeChunk(soundEffectSnowmanDeath);
	soundEffectSnowmanDeath = NULL;
	Mix_FreeChunk(soundEffectOuch);
	soundEffectOuch = NULL;
	Mix_FreeChunk(soundEffectSuccess);
	soundEffectSuccess = NULL;
	Mix_FreeChunk(soundEffectFailure);
	soundEffectFailure = NULL;
	Mix_FreeChunk(soundEffectPowerUp);
	soundEffectPowerUp = NULL;
}

void AudioManager::changeMusic(std::string path)
{
	if (Mix_PlayingMusic() == ZERO) {
		Mix_HaltMusic();
	}
	else if (Mix_PausedMusic() == ONE) {
		Mix_ResumeMusic();
	}
	Mix_FreeMusic(musicGame);
	musicGame = NULL;
	musicGame = Mix_LoadMUS(path.c_str());
	if (musicGame == NULL) {
		SDL_Log("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	else {
		Mix_FadeInMusic(musicGame, -ONE, MUSIC_FADE_IN_DELAY);
	}
}

void AudioManager::playSoundPlayerWalk()
{
	channelEffectPlayerWalk = Mix_PlayChannel(-ONE, soundEffectPlayerWalk, -ONE);
}

void AudioManager::stopSoundPlayerWalk()
{
	if (-ONE != channelEffectPlayerWalk) {
		Mix_HaltChannel(channelEffectPlayerWalk);
	}
}

void AudioManager::playSoundPlayerShoot()
{
	channelEffectPlayerShoot = Mix_PlayChannel(-ONE, soundEffectPlayerShoot, ZERO);
}

void AudioManager::playSoundSnowmanHit()
{
	channelEffectSnowmanHit = Mix_PlayChannel(-ONE, soundEffectSnowmanHit, ZERO);
}

void AudioManager::playSoundSnowmanBerserk()
{
	channelEffectSnowmanBerserk = Mix_PlayChannel(-ONE, soundEffectSnowmanBerserk, ZERO);
}

void AudioManager::playSoundSnowmanDeath()
{
	channelEffectSnowmanDeath = Mix_PlayChannel(-ONE, soundEffectSnowmanDeath, ZERO);
}

void AudioManager::playSoundOuch()
{
	channelEffectOuch = Mix_PlayChannel(-ONE, soundEffectOuch, ZERO);
}

void AudioManager::playSoundSuccess()
{
	channelEffectSuccess = Mix_PlayChannel(-ONE, soundEffectSuccess, ZERO);
}

void AudioManager::playSoundFailure()
{
	channelEffectFailure = Mix_PlayChannel(-ONE, soundEffectFailure, ZERO);
}

void AudioManager::playSoundPowerUp()
{
	channelEffectPowerUp = Mix_PlayChannel(-ONE, soundEffectPowerUp, ZERO);
}