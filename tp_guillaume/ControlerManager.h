#ifndef CONTROLERMANAGER_H
#define CONTROLERMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"

class ControlerManager
{
public:
	// Constructor
	ControlerManager();

	// Destructor
	~ControlerManager();

	// Methods
	void initialize(int &result);

private:
	// Parameters
	SDL_GameController* controller = NULL;
	SDL_Haptic* controllerHaptic = NULL;
};

#endif // !UTILS_H
