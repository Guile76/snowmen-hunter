#ifndef APPDEFINES_H
#define APPDEFINES_H

// Environment
#define WIDTH_SCREEN 1000
#define HEIGHT_SCREEN 600
#define WINDOW_RATIO_MODIFIER 0.9
#define WORLD_RADIUS 10000
#define WORLD_DEPTH -8000
#define WORLD_HEIGHT 12000
#define WORLD_FLOOR_REPEAT 100
#define COLOR_MAX 255
#define POP_LIMIT 1500
#define MIN_TREES 100
#define MAX_TREES 200
#define TREE_MIN_RADIUS 3
#define TREE_MAX_RADIUS 8
#define MIN_REFILL_POP_DELAY 40000
#define MAX_REFILL_POP_DELAY 80000

// Utils
#define FPS 60
#define SECOND 1000
#define MULTIPLIER_FULL 1.0f
#define MULTIPLIER_QUARTER 0.25f
#define MULTIPLIER_THREE_QUARTER 0.75f
#define MULTIPLIER_HALF 0.5f
#define MULTIPLIER_FIFTH 0.2f
#define MULTIPLIER_TENTH 0.1f
#define MULTIPLIER_TWENTIETH 0.05f
#define MULTIPLIER_FULL_AND_HALF 1.5f
#define MULTIPLIER_TWICE 2.0f
#define MULTIPLIER_THRICE 3.0f
#define MULTIPLIER_FOUR_TIME 4.0f
#define APP_MULTIPLIER_NEGATIVE -1
#define ZERO 0
#define ONE 1
#define NEGATIVE -1
#define RIGHT_ANGLE 90
#define RAD_BY_DEG 0.0174532925199

// Game
#define MARGIN 10
#define PAUSE_PRESS_DELAY 500
#define POINTER_WIDTH_ADAPT 7

// Player
#define JOYSTICK_DEAD_ZONE 8000
#define JOYSTICK_TRIGGER_MAX 32768
#define MOUSE_MODERATOR 10
#define PLAYER_VIEW_HEIGHT 30
#define PLAYER_SHOOT_DELAY 200
#define PLAYER_SPEED 1
#define PROJECTILE_SPEED 5
#define PROJECTILE_LIFESPAN 2000
#define PROJECTILE_COST 1
#define PLAYER_RADIUS 10
#define PLAYER_STARTING_LIVES 3
#define PLAYER_SCREEN_MARGIN 80
#define PLAYER_STARTING_SNOW 500
#define CANON_RECOIL 2
#define TANK_BODY_SIZE 10
#define TANK_SCREEN_SIZE 0.15f
#define SNOW_RUN_COST 1

// SnowMan
#define SNOWMAN_STARTING_NUMBER 10
#define SNOWMAN_GENERATION_RYTHM 20000
#define SNOWMAN_JUMP_MAX 10
#define SNOWMAN_RADIUS 10
#define SNOWMAN_HEALTH_POINTS 20
#define SNOWMAN_SPEED 1
#define SNOW_REFILL_RADIUS 10
#define SNOW_REFILL_COLOR_MIN 150
#define SNOW_REFILL_COLOR_MAX 255
#define SNOW_REFILL_BASE_COLOR 255
#define SNOW_REFILL_HOVER_VALUE 0.03
#define SNOW_REFILL_AMOUNT 100

// Files
#define SKYBOX_FLOOR "Images/sb_iceflow/iceflow_dn.jpg"
#define SKYBOX_BACK "Images/sb_iceflow/iceflow_bk.jpg"
#define SKYBOX_FORWARD "Images/sb_iceflow/iceflow_ft.jpg"
#define SKYBOX_LEFT "Images/sb_iceflow/iceflow_lf.jpg"
#define SKYBOX_RIGHT "Images/sb_iceflow/iceflow_rt.jpg"
#define SKYBOX_UP "Images/sb_iceflow/iceflow_up.jpg"

#define FONT_TITLE "Fonts/Christmas Snow.ttf"
#define FONT_MAIN "Fonts/PWHappyChristmas.ttf"
#define FONT_FLAKES "Fonts/Winter flakes.ttf"

// Texts
#define MULTIPLIER_FONT 0.05F
#define GAME_TITLE "Snowmen Hunter"
#define TEXT_MANUAL_FILE "Texts/manual.txt"
#define TEXT_BESTSCORE "Texts/bestscore.txt"
#define TEXT_PLAY "Play"
#define TEXT_MANUAL "Manual"
#define TEXT_OPTIONS "Options"
#define TEXT_EXIT "Exit"
#define TEXT_BACK "Back"
#define TEXT_GAME_OVER "GAME OVER"
#define TEXT_CONTROLLER "Controller : "
#define TEXT_CONTROLLER_KEYBOARD "keyboard and mouse"
#define TEXT_CONTROLLER_GAMEPAD "gamepad"
#define TEXT_PLUS "+"
#define TEXT_BESTCORE "Best score : "
#define TEXT_GAME_SPEED "Game speed : "

#define PLAYER_SCREEN_LIVES "Lives : "
#define PLAYER_SCREEN_SCORE "Score : "
#define PLAYER_SCREEN_SNOW_POOL "Snow pool : "

// Sounds
#define MUSIC_MAIN "Sounds/Musics/Santas Workshop.wav"
#define MUSIC_FADE_IN_DELAY 6000
#define SOUND_EFFECT_PLAYER_WALK "Sounds/Effects/engine_walk.wav"
#define SOUND_EFFECT_PLAYER_RUN "Sounds/Effects/engine_run.wav"
#define SOUND_EFFECT_PLAYER_SHOOT "Sounds/Effects/shoot.wav"
#define SOUND_EFFECT_SNOWMAN_HIT "Sounds/Effects/snowman_hit.wav"
#define SOUND_EFFECT_SNOWMAN_BERSERK "Sounds/Effects/snowman_berserk.wav"
#define SOUND_EFFECT_SNOWMAN_DEATH "Sounds/Effects/snowman_death.wav"
#define SOUND_EFFECT_LIFE_LOST "Sounds/Effects/ouch.wav"
#define SOUND_EFFECT_SUCCESS "Sounds/Effects/success.wav"
#define SOUND_EFFECT_FAILURE "Sounds/Effects/failure.wav"
#define SOUND_EFFECT_POWER_UP "Sounds/Effects/power_up.wav"

#endif // !DEFINES_H