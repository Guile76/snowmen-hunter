#include "TextsManager.h"

TextsManager::TextsManager(Game* game, SDL_Renderer* renderer, int windowHeight)
{
	this->game = game;
	this->renderer = renderer;
	textColor = { COLOR_MAX, COLOR_MAX, COLOR_MAX };
	fontSize = (int)(windowHeight * MULTIPLIER_FONT);
	setFont(FONT_MAIN, fontSize);
	initializeBestScore(TEXT_BESTSCORE);
}

TextsManager::~TextsManager()
{
	bgTexture.free();

	titleTexture.free();
	gameOverTexture.free();
	resultTexture.free();
	bestScoreTexture.free();

	buttonPlayTexture.free();
	buttonManualTexture.free();
	buttonExitTexture.free();
	buttonOptionsTexture.free();
	buttonBackTexture.free();
	buttonChooseControls.free();
	buttonGameSpeed.free();

	for each (AppTexture* t in manualText)
	{
		t->free();
	}
	manualText.clear();
}

AppTexture TextsManager::getButtonPlay()
{
	return buttonPlayTexture;
}

AppTexture TextsManager::getButtonManual()
{
	return buttonManualTexture;
}

AppTexture TextsManager::getButtonExit()
{
	return buttonExitTexture;
}

AppTexture TextsManager::getButtonOptions()
{
	return buttonOptionsTexture;
}

AppTexture TextsManager::getButtonBack()
{
	return buttonBackTexture;
}

AppTexture TextsManager::getButtonChooseControls()
{
	return buttonChooseControls;
}

AppTexture TextsManager::getBestScoreTexture()
{
	return bestScoreTexture;
}

AppTexture TextsManager::getButtonGameSpeed()
{
	return buttonGameSpeed;
}

int TextsManager::getBestScore()
{
	return bestScore;
}

void TextsManager::setFont(std::string path, int size)
{
	TTF_CloseFont(font);
	font = NULL;
	font = TTF_OpenFont(path.c_str(), size);
	if (NULL == font) {
		SDL_Log("Failed to load the font ! SDL_TTF Error: %s\n", TTF_GetError());
	}
}

void TextsManager::setTextTexture(AppTexture * texture, std::string text, int size, std::string fontPath, int red, int green, int blue)
{
	textColor = { (Uint8)red, (Uint8)green, (Uint8)blue };
	setFont(fontPath, size);
	texture->free();
	if (!texture->loadFromRenderedText(text, textColor, renderer, font)) {
		SDL_Log("Failed to text texture  : %s", text.c_str());
	}
	textColor = { COLOR_MAX, COLOR_MAX, COLOR_MAX };
	setFont(FONT_MAIN, fontSize);
}

void TextsManager::renderMenu(int windowWidth, int windowHeight)
{
	renderBG(windowWidth, windowHeight);

	titleTexture.render((int)(windowWidth * MULTIPLIER_HALF - titleTexture.getWidth() * MULTIPLIER_HALF), (int)(windowHeight * MULTIPLIER_QUARTER - titleTexture.getHeight() * MULTIPLIER_HALF), renderer);

	renderButton(fontSize, TEXT_PLAY, &buttonPlayTexture, M_START, MULTIPLIER_HALF, windowWidth, windowHeight);
	renderButton(fontSize, TEXT_MANUAL, &buttonManualTexture, M_MANUAL, MULTIPLIER_HALF, windowWidth, windowHeight);
	renderButton(fontSize, TEXT_OPTIONS, &buttonOptionsTexture, M_OPTIONS, MULTIPLIER_HALF, windowWidth, windowHeight);
	renderButton(fontSize, TEXT_EXIT, &buttonExitTexture, M_EXIT, MULTIPLIER_HALF, windowWidth, windowHeight);

	setTextTexture(&bestScoreTexture, TEXT_BESTCORE + std::to_string(bestScore), fontSize, FONT_FLAKES);
	bestScoreTexture.render((int)(windowWidth * MULTIPLIER_HALF - bestScoreTexture.getWidth() * MULTIPLIER_HALF), (int)(windowHeight * MULTIPLIER_THREE_QUARTER), renderer);
}

void TextsManager::renderManual(int windowWidth, int windowHeight)
{
	renderBG(windowWidth, windowHeight);

	renderButton(fontSize, TEXT_BACK, &buttonBackTexture, M_START, MULTIPLIER_TENTH, windowWidth, windowHeight);

	for (int i = ZERO; i < (int)manualText.size(); i++) {
		manualText[i]->render((int)(windowWidth * MULTIPLIER_HALF - manualText[i]->getWidth() * MULTIPLIER_HALF), (int)(windowHeight * MULTIPLIER_TENTH + titleTexture.getHeight() + (manualText[i]->getHeight() + MARGIN) * i + ONE), renderer);
	}
}

void TextsManager::renderOptions(int windowWidth, int windowHeight)
{
	renderBG(windowWidth, windowHeight);

	renderButton(fontSize, TEXT_BACK, &buttonBackTexture, M_START, MULTIPLIER_TENTH, windowWidth, windowHeight);

	displayedController = game->isGameControllerActive ? TEXT_CONTROLLER_GAMEPAD : TEXT_CONTROLLER_KEYBOARD;
	renderButton(fontSize, TEXT_CONTROLLER + displayedController, &buttonChooseControls, M_START, MULTIPLIER_QUARTER, windowWidth, windowHeight, FONT_FLAKES);

	switch (game->gameSpeed)
	{
	case GS_NORMAL:
		displayedGameSpeed = "Normal";
		break;
	case GS_FAST:
		displayedGameSpeed = "Fast";
		break;
	case GS_FASTER:
		displayedGameSpeed = "Faster";
		break;
	case GS_VERY_FAST:
		displayedGameSpeed = "Very Fast";
		break;
	}
	renderButton(fontSize, TEXT_GAME_SPEED + displayedGameSpeed, &buttonGameSpeed, M_MANUAL, MULTIPLIER_QUARTER, windowWidth, windowHeight, FONT_FLAKES);
}

void TextsManager::print(int windowWidth, int windowHeight, int posX, int posY, std::string text, void* font, int red, int green, int blue)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(ZERO, (GLdouble)windowWidth, ZERO, (GLdouble)windowHeight);
	glColor3ub(red, green, blue);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glRasterPos2i((GLint)posX, (GLint)posY);
	const unsigned char* t = reinterpret_cast<const unsigned char *>(text.c_str());
	glutBitmapString(font, t);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glColor3ub(COLOR_MAX, COLOR_MAX, COLOR_MAX);
}

void TextsManager::displayGameInfo(int windowWidth, int windowHeight)
{
	// Draw snow pool
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_TWENTIETH), (int)(windowHeight - PLAYER_SCREEN_MARGIN), PLAYER_SCREEN_SNOW_POOL + std::to_string(game->getPlayer()->getSnowPool()), GLUT_BITMAP_TIMES_ROMAN_24, ZERO, ZERO, ZERO);
	// Draw lives
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_TWENTIETH), (int)(windowHeight - PLAYER_SCREEN_MARGIN * MULTIPLIER_TWICE), PLAYER_SCREEN_LIVES + std::to_string(game->getPlayer()->getLives()), GLUT_BITMAP_TIMES_ROMAN_24, ZERO, ZERO, ZERO);
	// Draw score
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_TWENTIETH), (int)(windowHeight - PLAYER_SCREEN_MARGIN * MULTIPLIER_THRICE), PLAYER_SCREEN_SCORE + std::to_string(game->getScore()), GLUT_BITMAP_TIMES_ROMAN_24, ZERO, ZERO, ZERO);
}

void TextsManager::drawPointer(int windowWidth, int windowHeight)
{
	// Draw pointer
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - POINTER_WIDTH_ADAPT), (int)(windowHeight * MULTIPLIER_HALF), TEXT_PLUS, GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
}

void TextsManager::drawPauseText(int windowWidth, int windowHeight)
{
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 35), (int)(windowHeight * MULTIPLIER_QUARTER), "PAUSE", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 160), (int)(windowHeight * MULTIPLIER_QUARTER - 60), "Press Back or Escape to go to menu", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 150), (int)(windowHeight * MULTIPLIER_QUARTER - 120), "Press Start or Space to continue", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
}

void TextsManager::drawGameOverText(int windowWidth, int windowHeight)
{
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 45), (int)(windowHeight * MULTIPLIER_QUARTER), "Game Over", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 160), (int)(windowHeight * MULTIPLIER_QUARTER - 60), "Press Back or Escape to go to menu", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
	print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 110), (int)(windowHeight * MULTIPLIER_QUARTER - 120), "Press Space or Start to retry", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);

	if (game->getScore() > getBestScore()) {
		print(windowWidth, windowHeight, (int)(windowWidth * MULTIPLIER_HALF - 180), (int)(windowHeight * MULTIPLIER_QUARTER - 180), "Congratulations, you beat the high score", GLUT_BITMAP_TIMES_ROMAN_24, COLOR_MAX, ZERO, ZERO);
		setBestScore(TEXT_BESTSCORE, game->getScore());
	}
}

void TextsManager::renderBG(int windowWidth, int windowHeight)
{
	bgTexture.render(ZERO, ZERO, renderer, new SDL_Rect({ ZERO, ZERO, windowWidth, windowHeight }));
}

void TextsManager::renderButton(int fontsize, std::string text, AppTexture * button, EMenu position, float height, int windowWidth, int windowHeight, std::string fontPath)
{
	game->getMouseManager()->checkIsInButton(*button) ? setTextTexture(button, text, fontsize, fontPath, COLOR_MAX, ZERO, ZERO) : setTextTexture(button, text, fontsize, fontPath);
	button->render((int)(windowWidth * MULTIPLIER_HALF - button->getWidth() * MULTIPLIER_HALF), (int)(windowHeight * height + (button->getHeight() + MARGIN) * position), renderer);
}

void TextsManager::initializeTextsTextures()
{
	bgTexture.loadFromFile(SKYBOX_FLOOR, renderer, MULTIPLIER_FULL);

	setTextTexture(&titleTexture, GAME_TITLE, (int)(fontSize * MULTIPLIER_THRICE), FONT_TITLE, 100, ZERO, ZERO);
	setTextTexture(&gameOverTexture, TEXT_GAME_OVER, (int)(fontSize * MULTIPLIER_TWICE));

	setTexts(TEXT_MANUAL_FILE, &manualText, (int)(fontSize * MULTIPLIER_HALF), FONT_MAIN, ZERO, ZERO, 50);
}

void TextsManager::setTexts(std::string path, std::vector<AppTexture*>* texts, int size, std::string fontPath, int red, int green, int blue)
{
	std::string manualFile = path;
	std::ifstream fileIn(manualFile.c_str());
	std::string line;
	int lineCmp = ZERO;
	while (std::getline(fileIn, line)) {
		texts->push_back(new AppTexture());
		setTextTexture(texts->at(lineCmp), line, size, fontPath, red, green, blue);
		lineCmp++;
	}
}

void TextsManager::initializeBestScore(std::string path)
{
	std::string manualFile = path;
	std::ifstream fileIn(manualFile.c_str());
	std::string line;
	std::getline(fileIn, line);
	std::string::size_type sz;
	bestScore = std::stoi(line, &sz);
	fileIn.close();
}

void TextsManager::setBestScore(std::string path, int newBestScore)
{
	std::string manualFile = path;
	std::ofstream fileOut(manualFile.c_str());
	if (fileOut) {
		fileOut << std::to_string(newBestScore);
	}
	else {
		SDL_Log("Unable to write into %s", path);
	}
	fileOut.close();
}