#ifndef KEYBOARDMANAGER_H
#define KEYBOARDMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"
#include "Game.h"

class Game;

class KeyboardManager
{
public:
	// Constructor
	KeyboardManager(Game* game);

	// Destructor
	~KeyboardManager();

	// Method
	void controlGame();

private:
	// Parameters
	Game* game;
};
#endif // !KEYBOARDMANAGER_H
