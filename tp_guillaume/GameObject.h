#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "AppIncludes.h"
#include "AppDefines.h"
#include "AppEnums.h"

class GameObject
{
public:
	// Constructor
	GameObject(double radius);

	// Destructor
	~GameObject();

	// Variables
	bool isActive;
	double speed;

	// Methods
	virtual void render() = 0;
	SDL_Rect* getBounds();
	double getPosX();
	double getPosY();
	void setSize();
	void moveForward();
	void moveBackward();
	void moveLeft();
	void moveRight();
	void turnLeft(int value = JOYSTICK_TRIGGER_MAX);
	void turnRight(int value = JOYSTICK_TRIGGER_MAX);

protected:
	// Constructor
	GameObject();

	// Parameters
	SDL_Rect* bounds;

	// Variables
	double posX, posY, posZ, radius, angle;
};

#endif // !GAMEOBJECT_H
