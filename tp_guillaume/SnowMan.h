#ifndef SNOWMAN_H
#define SNOWMAN_H

#include "GameObject.h"
#include "Game.h"
#include "AppDefines.h"
#include "AppEnums.h"
#include "Utils.h"

class Game;

class SnowMan :
	public GameObject
{
public:
	// Constructor
	SnowMan(double x, double y, Game* game, double radius);

	// Destructor
	~SnowMan();

	// Variables
	bool isBerserk;

	// Methods
	void render() override;
	void move();
	bool removeHPAndCheckDeath();
	bool checkBerserk();

private:
	// Constructor
	SnowMan();

	// Parameter
	Game* game;

	// Variables
	double posZ;
	bool isActing, isGoingUp;
	int startActingTick, healthPoints;
	ESnowManActions currentAction;
	GLubyte red, green, blue;

	Utils::MyVector vectorSnowManToFace, vectorSnowManToSide, vectorSnowManToPlayer;
	double cosAngleFace, thisAngleFace, cosAngleSide, thisAngleSide;
	int handed;

	// Methods
	void jump();
	bool checkAboutToCollide(EDirections direction);
	void actNormal();
	void actBerserk();
};

#endif // !PLAYER_H
