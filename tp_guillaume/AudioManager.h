#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include "AppIncludes.h"
#include "AppDefines.h"

class AudioManager
{
public:
	// Consctructor
	AudioManager();

	// Destructor
	~AudioManager();

	// Methods
	void initialize();
	void freeMemory();

	void changeMusic(std::string path);

	void playSoundPlayerWalk();
	void stopSoundPlayerWalk();

	void playSoundPlayerShoot();
	void playSoundSnowmanHit();
	void playSoundSnowmanBerserk();
	void playSoundSnowmanDeath();
	void playSoundOuch();
	void playSoundSuccess();
	void playSoundFailure();
	void playSoundPowerUp();

private:
	// Parameters
	Mix_Music* musicGame = NULL;
	Mix_Chunk *soundEffectPlayerWalk, *soundEffectPlayerShoot, *soundEffectPlayerRun, *soundEffectSnowmanHit, *soundEffectSnowmanBerserk, *soundEffectSnowmanDeath, *soundEffectOuch, *soundEffectSuccess, *soundEffectFailure, *soundEffectPowerUp = NULL;
	int channelEffectPlayerWalk, channelEffectPlayerShoot, channelEffectPlayerRun, channelEffectSnowmanHit, channelEffectSnowmanBerserk, channelEffectSnowmanDeath, channelEffectOuch, channelEffectSuccess, channelEffectFailure, channelEffectPowerUp;
};

#endif // !AUDIOMANAGER_H
