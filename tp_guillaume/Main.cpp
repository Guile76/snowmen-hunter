#include "Game.h"

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	int result = ZERO;
	Game* g = new Game();
	result = g->initialize();

	if (result == ZERO) {
		g->execute();
	}
	g->freeMemory();

	return result;
}