#include "ControlerManager.h"

ControlerManager::ControlerManager()
{
}

ControlerManager::~ControlerManager()
{
}

void ControlerManager::initialize(int &result)
{
	controller = NULL;
	bool isFound = false;
	int joystickCmp = ZERO;

	// Search the first connected controller
	if (SDL_NumJoysticks()) {
		while (!isFound && joystickCmp < SDL_NumJoysticks()) {
			if (SDL_IsGameController(joystickCmp)) {
				controller = SDL_GameControllerOpen(joystickCmp);
				if (!controller) {
					SDL_Log("Could not open gamecontroller %i: %s\n", joystickCmp, SDL_GetError());
				}
				else {
					isFound = true;
					controllerHaptic = SDL_HapticOpen(ZERO);
					if (controllerHaptic == NULL) {
						SDL_Log("Could not open controllerHaptic");
					}
					else {
						if (SDL_HapticRumbleSupported(controllerHaptic)) {
							if (SDL_HapticRumbleInit(controllerHaptic) != ZERO) {
								SDL_Log("Could not initialize rumbler");
							}
						}
						else {
							SDL_Log("Rumble not supported");
						}
					}
				}
			}
			else {
				joystickCmp++;
			}
		}
	}
	else {
		SDL_Log("No joystick found.");
	}
}