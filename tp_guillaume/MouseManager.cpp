#include "MouseManager.h"

MouseManager::MouseManager(Game* game)
{
	this->game = game;
}

MouseManager::~MouseManager()
{
}

int MouseManager::getMouseX()
{
	return mouseX;
}

int MouseManager::getMouseY()
{
	return mouseY;
}

void MouseManager::initialize()
{
	SDL_SetWindowGrab(game->getGameWindow(), SDL_TRUE);
	centerMouse();
}

void MouseManager::controlGame()
{
	if (game->getPlayer()->isAlive && !game->isPaused) {
		int x, y;
		SDL_PumpEvents();
		SDL_GetMouseState(&x, &y);

		mouseMotion = x < ZERO ? x * NEGATIVE : x;
		mouseMotion = (int)((mouseMotion - game->getWindowWidth() * MULTIPLIER_HALF) * JOYSTICK_TRIGGER_MAX / MOUSE_MODERATOR);

		if (x < ZERO) {
			game->getPlayer()->turnLeft(mouseMotion);
		}
		else if (x > ZERO) {
			game->getPlayer()->turnRight(mouseMotion);
		}

		if (S_GAME == game->getScene() && ((game->getWindowWidth() * MULTIPLIER_HALF) != x || (game->getWindowHeight() * MULTIPLIER_HALF != y))) {
			centerMouse();
		}
		mouseMotion = ZERO;

		if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
			game->getPlayer()->isShooting = true;
		}
		else {
			game->getPlayer()->isShooting = false;
		}
	}
}

void MouseManager::controlMenu()
{
	int infoReturn;
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
		switch (game->getScene())
		{
		case S_HOME:
			if (checkIsInButton(game->getTextsManager()->getButtonPlay())) {
				game->changeScene(S_GAME, infoReturn);
			}
			if (checkIsInButton(game->getTextsManager()->getButtonManual())) {
				game->changeScene(S_MANUAL, infoReturn);
			}

			if (checkIsInButton(game->getTextsManager()->getButtonOptions())) {
				game->changeScene(S_OPTIONS, infoReturn);
			}
			if (checkIsInButton(game->getTextsManager()->getButtonExit())) {
				game->isRunning = false;
			}
			break;
		case S_MANUAL:
			if (checkIsInButton(game->getTextsManager()->getButtonBack())) {
				game->changeScene(S_HOME, infoReturn);
			}
			break;
		case S_OPTIONS:
			if (checkIsInButton(game->getTextsManager()->getButtonBack())) {
				game->changeScene(S_HOME, infoReturn);
			}
			if (game->hasController && checkIsInButton(game->getTextsManager()->getButtonChooseControls())) {
				game->isGameControllerActive = !game->isGameControllerActive;
			}
			if (checkIsInButton(game->getTextsManager()->getButtonGameSpeed())) {
				switch (game->gameSpeed)
				{
				case GS_NORMAL:
					game->gameSpeed = GS_FAST;
					break;
				case GS_FAST:
					game->gameSpeed = GS_FASTER;
					break;
				case GS_FASTER:
					game->gameSpeed = GS_VERY_FAST;
					break;
				case GS_VERY_FAST:
					game->gameSpeed = GS_NORMAL;
					break;
				}
			}

			break;
		}
	}
}

void MouseManager::centerMouse()
{
	SetCursorPos((int)(game->getWindowWidth() * MULTIPLIER_HALF), (int)(game->getWindowHeight() * MULTIPLIER_HALF));
}

bool MouseManager::checkIsInButton(AppTexture button)
{
	bool result = false;

	if (mouseX > button.getRenderQuad().x && mouseX < button.getRenderQuad().x + button.getRenderQuad().w && mouseY > button.getRenderQuad().y && mouseY < button.getRenderQuad().y + button.getRenderQuad().h) {
		result = true;
	}

	return result;
}

void MouseManager::hideCursor(bool on)
{
	SDL_ShowCursor(on ? SDL_DISABLE : SDL_ENABLE);
}

void MouseManager::updateMousePosition()
{
	SDL_GetMouseState(&mouseX, &mouseY);
}