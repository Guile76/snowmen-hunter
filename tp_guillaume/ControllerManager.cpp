#include "ControllerManager.h"

ControllerManager::ControllerManager(Game* game)
{
	this->game = game;
}

ControllerManager::~ControllerManager()
{
	SDL_HapticClose(controllerHaptic);
	SDL_GameControllerClose(controller);
	controllerHaptic = NULL;
	controller = NULL;
}

void ControllerManager::initialize(int & result)
{
	if (SDL_Init(SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC) < ZERO)
	{
		SDL_Log("SDL initialization failed. SDL Error : %s", SDL_GetError());
		result = EXIT_FAILURE;
	}

	bool isFound = false;
	int joystickCmp = ZERO;

	// Search the first connected controller
	if (SDL_NumJoysticks()) {
		while (!isFound && joystickCmp < SDL_NumJoysticks()) {
			if (SDL_IsGameController(joystickCmp)) {
				controller = SDL_GameControllerOpen(joystickCmp);
				if (!controller) {
					SDL_Log("Could not open gamecontroller %i: %s\n", joystickCmp, SDL_GetError());
				}
				else {
					isFound = true;
					controllerHaptic = SDL_HapticOpen(ZERO);
					if (NULL == controllerHaptic) {
						SDL_Log("Could not open controllerHaptic");
					}
					else {
						if (SDL_HapticRumbleSupported(controllerHaptic)) {
							if (SDL_HapticRumbleInit(controllerHaptic) != ZERO) {
								SDL_Log("Could not initialize rumbler");
							}
						}
						else {
							SDL_Log("Rumble not supported");
						}
					}
				}
			}
			else {
				joystickCmp++;
			}
		}
		game->hasController = true;
	}
	else {
		game->hasController = false;
		SDL_Log("No joystick found.");
	}
}

void ControllerManager::controlGame()
{
	if (game->getPlayer()->isAlive && !game->isPaused) {
		if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTX) < -JOYSTICK_DEAD_ZONE) {
			game->getPlayer()->authMoveLeft();
		}
		else if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTX) > JOYSTICK_DEAD_ZONE) {
			game->getPlayer()->authMoveRight();
		}

		if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) < -JOYSTICK_DEAD_ZONE) {
			game->getPlayer()->authMoveForward();
		}
		else if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY) > JOYSTICK_DEAD_ZONE) {
			game->getPlayer()->authMoveBackward();
		}

		if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX) < JOYSTICK_DEAD_ZONE * NEGATIVE) {
			game->getPlayer()->turnLeft(SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX) * NEGATIVE);
		}
		else if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX) > JOYSTICK_DEAD_ZONE) {
			game->getPlayer()->turnRight(SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_RIGHTX));
		}

		if (game->getPlayer()->isAllowedToShot) {
			if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) > ZERO) {
				game->getPlayer()->isShooting = true;
			}
			else {
				game->getPlayer()->isShooting = false;
			}
		}

		if (SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_TRIGGERLEFT) > ZERO) {
			game->getPlayer()->run();
		}
		else {
			game->getPlayer()->walk();
		}
	}

	if ((game->isPaused || game->isLost) && SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_BACK)) {
		game->changeScene(S_HOME, game->result);
	}

	if (SDL_GameControllerGetButton(controller, SDL_CONTROLLER_BUTTON_START)) {
		!game->isLost ? game->togglePause() : game->changeScene(S_GAME, game->result);
	}
}