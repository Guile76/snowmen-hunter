#include "GameObject.h"

GameObject::GameObject()
{
}

void GameObject::render()
{
}

SDL_Rect * GameObject::getBounds()
{
	return bounds;
}

double GameObject::getPosX()
{
	return posX;
}

double GameObject::getPosY()
{
	return posY;
}

void GameObject::setSize()
{
	delete(bounds);
	bounds = NULL;
	bounds = new SDL_Rect({ (int)(posX - radius * MULTIPLIER_HALF), (int)(posY - radius * MULTIPLIER_HALF), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
}

GameObject::GameObject(double radius)
{
	posX = posY = angle = posZ = ZERO;
	this->radius = radius;
	speed = ONE;
	setSize();
	isActive = true;
}

GameObject::~GameObject()
{
	delete(bounds);
	bounds = NULL;
}

void GameObject::moveForward()
{
	posX += speed * cos(angle * RAD_BY_DEG);
	posY += speed * sin(angle * RAD_BY_DEG);
	setSize();
}

void GameObject::moveBackward()
{
	posX -= speed * cos(angle * RAD_BY_DEG);
	posY -= speed * sin(angle * RAD_BY_DEG);
	setSize();
}

void GameObject::moveLeft()
{
	posX += speed * cos((angle + RIGHT_ANGLE) * RAD_BY_DEG);
	posY += speed * sin((angle + RIGHT_ANGLE) * RAD_BY_DEG);
	setSize();
}

void GameObject::moveRight()
{
	posX += speed * cos((angle - RIGHT_ANGLE) * RAD_BY_DEG);
	posY += speed * sin((angle - RIGHT_ANGLE) * RAD_BY_DEG);
	setSize();
}

void GameObject::turnLeft(int value)
{
	angle += speed * value / JOYSTICK_TRIGGER_MAX;
}

void GameObject::turnRight(int value)
{
	angle -= speed * value / JOYSTICK_TRIGGER_MAX;
}