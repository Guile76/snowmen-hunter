#include "Player.h"

Player::Player()
{
}

bool Player::checkAboutToCollide(EDirections direction)
{
	SDL_Rect* testRect = NULL;

	bool result = false;
	switch (direction)
	{
	case LEFT:
		testRect = new SDL_Rect({ (int)(posX + (radius * MULTIPLIER_TWICE) * cos((angle + RIGHT_ANGLE) * RAD_BY_DEG)), (int)(posY + (radius * MULTIPLIER_TWICE) * sin((angle + RIGHT_ANGLE) * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	case RIGHT:
		testRect = new SDL_Rect({ (int)(posX + (radius * MULTIPLIER_TWICE) * cos((angle - RIGHT_ANGLE) * RAD_BY_DEG)), (int)(posY + (radius * MULTIPLIER_TWICE) * sin((angle - RIGHT_ANGLE) * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	case FORWARD:
		testRect = new SDL_Rect({ (int)(posX + (radius * MULTIPLIER_TWICE) * cos(angle * RAD_BY_DEG)), (int)(posY + (radius * MULTIPLIER_TWICE) * sin(angle * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	case BACKWARD:
		testRect = new SDL_Rect({ (int)(posX - (radius * MULTIPLIER_TWICE) * cos(angle * RAD_BY_DEG)), (int)(posY - (radius * MULTIPLIER_TWICE) * sin(angle * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	}

	int i = ZERO;

	while (i < (int)game->getEnemiesManager()->enemies.size() && !result) {
		if (SDL_HasIntersection(testRect, game->getEnemiesManager()->enemies[i]->getBounds())) {
			result = true;
		}
		else {
			i++;
		}
	}

	delete(testRect);
	testRect = NULL;

	return result;
}

Player::Player(Game* game, double radius) : GameObject(radius)
{
	targetHeight = viewHeight = PLAYER_VIEW_HEIGHT;
	this->game = game;
	posZ = viewHeight;
	reset();
}

Player::~Player()
{
}

void Player::setGameView()
{
	glLoadIdentity();
	gluLookAt(posX, posY, posZ, posX + cos(-angle * RAD_BY_DEG), posY - sin(-angle * RAD_BY_DEG), targetHeight, ZERO, ZERO, ONE);
}

void Player::setGameCamera()
{
	glLoadIdentity();
	glRotated(-angle + RIGHT_ANGLE * MULTIPLIER_HALF, ZERO, ZERO, ONE);
	gluLookAt(posX, posY, 400, posX + ONE, posY + ONE, ZERO, ZERO, ZERO, ONE);
}

void Player::render()
{
	glPushMatrix();
	glTranslated(posX, posY, viewHeight);
	glRotated(angle, ZERO, ZERO, ONE);
	ONE == canonMultiplier ? glCallList(game->getObjectRenderer()->getCanonRight()) : glCallList(game->getObjectRenderer()->getCanonLeft());
	glCallList(game->getObjectRenderer()->getTank());
	glPopMatrix();
}

void Player::shot()
{
	if (isAllowedToShot && isShooting && (int)SDL_GetTicks() >= shootDelayTick + PLAYER_SHOOT_DELAY && checkAndReduceSnowPool(PROJECTILE_COST)) {
		playerProjectiles.push_back(new Projectile(posX + 5 * cos(angle * RAD_BY_DEG) + 4 * canonMultiplier * cos((angle + RIGHT_ANGLE) * RAD_BY_DEG), posY + 5 * sin(angle * RAD_BY_DEG) + 4 * canonMultiplier * sin((angle + RIGHT_ANGLE) * RAD_BY_DEG), viewHeight - 3, angle, game, 1));

		canonMultiplier *= -ONE;

		game->getAudioManager()->playSoundPlayerShoot();

		shootDelayTick = SDL_GetTicks();
	}
}

void Player::setProjectiles()
{
	for (int i = ZERO; i < (int)playerProjectiles.size(); i++) {
		if (!isActive || (long int)SDL_GetTicks() > playerProjectiles[i]->getBirthTime() + PROJECTILE_LIFESPAN) {
			playerProjectiles[i]->~Projectile();
			playerProjectiles.erase(playerProjectiles.begin() + i);
		}
		else {
			playerProjectiles[i]->moveForward();
		}
	}
}

void Player::renderProjectiles()
{
	for (int i = ZERO; i < (int)playerProjectiles.size(); i++) {
		if (playerProjectiles[i]->isActive) {
			playerProjectiles[i]->render();
		}
	}
}

void Player::authMoveForward()
{
	if (!checkAboutToCollide(FORWARD)) {
		if (!isRunning || (isRunning && checkAndReduceSnowPool(SNOW_RUN_COST))) {
			moveForward();
		}
	}
}

void Player::authMoveBackward()
{
	if (!checkAboutToCollide(BACKWARD)) {
		if (!isRunning || (isRunning && checkAndReduceSnowPool(SNOW_RUN_COST))) {
			moveBackward();
		}
	}
}

void Player::authMoveLeft()
{
	if (!checkAboutToCollide(LEFT)) {
		if (!isRunning || (isRunning && checkAndReduceSnowPool(SNOW_RUN_COST))) {
			moveLeft();
		}
	}
}

void Player::authMoveRight()
{
	if (!checkAboutToCollide(RIGHT)) {
		if (!isRunning || (isRunning && checkAndReduceSnowPool(SNOW_RUN_COST))) {
			moveRight();
		}
	}
}

void Player::setAngle(int angle)
{
	this->angle = angle;
}

void Player::walk()
{
	isRunning = false;
	speed = (int)game->gameSpeed * MULTIPLIER_TWICE * PLAYER_SPEED;
}

void Player::run()
{
	isRunning = true;
	speed = (int)game->gameSpeed * MULTIPLIER_TWICE * PLAYER_SPEED * MULTIPLIER_TWICE;
}

int Player::getLives()
{
	return lives;
}

void Player::addLife()
{
	lives++;
}

bool Player::removeLifeAndCheckDeath()
{
	bool result = false;
	lives--;
	if (lives <= ZERO) {
		result = true;
	}
	return result;
}

int Player::getSnowPool()
{
	return snowPool;
}

void Player::increaseSnowPool(int value)
{
	snowPool += value;
}

bool Player::checkAndReduceSnowPool(int value)
{
	bool result = snowPool - value < ZERO ? false : true;

	if (result) {
		snowPool -= value;
	}

	return result;
}

void Player::reset()
{
	this->posX = this->posY = this->angle = ZERO;
	isAllowedToShot = isAlive = true;
	isShooting = false;
	canonMultiplier = ONE;
	walk();
	lives = PLAYER_STARTING_LIVES;
	snowPool = PLAYER_STARTING_SNOW;
}