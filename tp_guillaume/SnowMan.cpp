#include "SnowMan.h"

SnowMan::SnowMan()
{
}

SnowMan::SnowMan(double x, double y, Game * game, double radius) : GameObject(radius)
{
	this->game = game;
	this->posZ = ZERO;
	this->posX = x;
	this->posY = y;
	setSize();
	this->healthPoints = SNOWMAN_HEALTH_POINTS;

	this->isActing = false;
	this->isGoingUp = true;
	this->red = this->green = 200;
	this->blue = COLOR_MAX;

	this->speed = (int)game->gameSpeed * MULTIPLIER_TWICE * SNOWMAN_SPEED;
	this->isBerserk = false;

	handed = Utils::getRandomInt(ZERO, ONE);
}

SnowMan::~SnowMan()
{
}

void SnowMan::render()
{
	glPushMatrix();
	glTranslated(posX, posY, posZ);
	glRotated(angle, ZERO, ZERO, ONE);
	glColor3ub(red, green, blue);
	glCallList(game->getObjectRenderer()->getSnowMan());
	glColor3ub(COLOR_MAX, COLOR_MAX, COLOR_MAX);
	glPopMatrix();
}

void SnowMan::move()
{
	if (isBerserk) {
		actBerserk();
	}
	else {
		actNormal();
	}
}

bool SnowMan::removeHPAndCheckDeath()
{
	bool result = false;
	healthPoints--;
	if (healthPoints <= ZERO) {
		result = true;
		game->getObjectsManager()->generateRefills((int)this->getPosX(), (int)this->getPosY());
		delete(this);
	}
	else {
		if (red < COLOR_MAX) {
			red += 5;
			if (red > COLOR_MAX) {
				red = COLOR_MAX;
			}
		}
		blue -= 11;
		if (blue <= green) {
			green -= 11;
		}
	}

	speed += ONE * MULTIPLIER_QUARTER;
	return result;
}

void SnowMan::jump()
{
	if (isGoingUp) {
		posZ < SNOWMAN_JUMP_MAX ? posZ++ : isGoingUp = false;
	}
	else {
		posZ > ZERO ? posZ-- : isGoingUp = true;
	}
}

bool SnowMan::checkAboutToCollide(EDirections direction)
{
	SDL_Rect* testRect = NULL;

	bool result = false;

	switch (direction)
	{
	case LEFT:
		testRect = new SDL_Rect({ (int)(posX + (radius * MULTIPLIER_TWICE) * cos((angle + RIGHT_ANGLE) * RAD_BY_DEG)), (int)(posY + (radius * MULTIPLIER_TWICE) * sin((angle + RIGHT_ANGLE) * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	case RIGHT:
		testRect = new SDL_Rect({ (int)(posX + (radius * MULTIPLIER_TWICE) * cos((angle - RIGHT_ANGLE) * RAD_BY_DEG)), (int)(posY + (radius * MULTIPLIER_TWICE) * sin((angle - RIGHT_ANGLE) * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	case FORWARD:
		testRect = new SDL_Rect({ (int)(posX + (radius * MULTIPLIER_TWICE) * cos(angle * RAD_BY_DEG)), (int)(posY + (radius * MULTIPLIER_TWICE) * sin(angle * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	case BACKWARD:
		testRect = new SDL_Rect({ (int)(posX - (radius * MULTIPLIER_TWICE) * cos(angle * RAD_BY_DEG)), (int)(posY - (radius * MULTIPLIER_TWICE) * sin(angle * RAD_BY_DEG)), (int)(radius * MULTIPLIER_TWICE), (int)(radius * MULTIPLIER_TWICE) });
		break;
	}

	int i = ZERO;

	while (i < (int)game->getEnemiesManager()->enemies.size() && !result) {
		if (SDL_HasIntersection(testRect, game->getEnemiesManager()->enemies[i]->getBounds())) {
			result = true;
		}
		else {
			i++;
		}
	}

	delete(testRect);
	testRect = NULL;

	return result;
}

void SnowMan::actNormal()
{
	if (!isActing) {
		startActingTick = (int)SDL_GetTicks();
		int ran = rand() % 9;
		switch (ran)
		{
		case 0:
			currentAction = SM_WAIT;
			break;
		case 1:
		case 2:
			currentAction = SM_TURN_LEFT;
			break;
		case 3:
		case 4:
			currentAction = SM_TURN_RIGHT;
			break;
		default:
			currentAction = SM_FORWARD;
			break;
		}
		isActing = true;
	}
	else {
		if ((int)SDL_GetTicks() < startActingTick + 1000) {
			switch (currentAction)
			{
			case SM_WAIT:
				break;
			case SM_TURN_LEFT:
				turnLeft();
				break;
			case SM_TURN_RIGHT:
				turnRight();
				break;
			default:
				if (!checkAboutToCollide(FORWARD)) {
					moveForward();
				}
				break;
			}
			jump();
		}
		else {
			isActing = false;
		}
	}
}

void SnowMan::actBerserk()
{
	vectorSnowManToFace.set((posX + SNOWMAN_RADIUS * cos(angle * RAD_BY_DEG)) - posX, (posY + SNOWMAN_RADIUS * sin(angle * RAD_BY_DEG)) - posY, Utils::getDistance(posX, posY, posX + SNOWMAN_RADIUS * cos(angle * RAD_BY_DEG), posY + SNOWMAN_RADIUS * sin(angle * RAD_BY_DEG)));

	vectorSnowManToSide.set((posX + SNOWMAN_RADIUS * cos((angle + RIGHT_ANGLE) * RAD_BY_DEG)) - posX, (posY + SNOWMAN_RADIUS * sin((angle + RIGHT_ANGLE) * RAD_BY_DEG)) - posY, Utils::getDistance(posX, posY, posX + SNOWMAN_RADIUS * cos((angle + RIGHT_ANGLE) * RAD_BY_DEG), posY + SNOWMAN_RADIUS * sin((angle + RIGHT_ANGLE) * RAD_BY_DEG)));

	vectorSnowManToPlayer.set(game->getPlayer()->getPosX() - posX, game->getPlayer()->getPosY() - posY, sqrt((game->getPlayer()->getPosX() - posX) * (game->getPlayer()->getPosX() - posX) + (game->getPlayer()->getPosY() - posY) * (game->getPlayer()->getPosY() - posY)));

	cosAngleFace = ((vectorSnowManToFace.x * vectorSnowManToPlayer.x) + (vectorSnowManToFace.y * vectorSnowManToPlayer.y)) / (vectorSnowManToFace.distance * vectorSnowManToPlayer.distance);
	// if positive => front

	thisAngleFace = acos(cosAngleFace);

	cosAngleSide = ((vectorSnowManToSide.x * vectorSnowManToPlayer.x) + (vectorSnowManToSide.y * vectorSnowManToPlayer.y)) / (vectorSnowManToSide.distance * vectorSnowManToPlayer.distance);
	// if positive => left
	// +0 = face

	thisAngleSide = acos(cosAngleSide);

	if ((float)cosAngleSide >= 0.1f) {
		turnLeft();
	}
	else if ((float)cosAngleSide <= -0.1f) {
		turnRight();
	}

	if ((float)cosAngleFace > ZERO) {
		if (!checkAboutToCollide(FORWARD)) {
			moveForward();
		}
		else {
			handed == ZERO ? moveLeft() : moveRight();
		}
	}

	jump();
}

bool SnowMan::checkBerserk()
{
	bool result = false;
	if (healthPoints <= SNOWMAN_HEALTH_POINTS * MULTIPLIER_HALF) {
		result = true;
		game->getAudioManager()->playSoundSnowmanBerserk();
	}
	return result;
}