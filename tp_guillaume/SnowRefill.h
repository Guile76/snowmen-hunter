#ifndef SNOWREFILL_H
#define SNOWREFILL_H

#include "GameObject.h"
#include "Game.h"

class Game;

class SnowRefill :
	public GameObject
{
public:
	// Constructor
	SnowRefill(Game* game, int x, int y);

	// Destructor
	~SnowRefill();

	// Method
	void render();

private:
	// Parameter
	Game* game;

	// Variable
	int colorVariation;
	bool colorDown;
	double hoverValue;
};

#endif // !SNOWREFILL_H
